#include "fox32.h"

#include <chnlib/str.h>
#include <chnlib/strbuffer.h>
#include <chnlib/logger.h>
#include <chnlib/map.h>


typedef struct StringLitData {
    u32 id;
    String val;
} StringLitData;

typedef struct CodegenCtx {
    Compiler  *compiler;
    MirCtx    *mir;

    StrBuffer  strbuf;

    u32 tmp_reg;

    Map(StringLitData) strings;
} CodegenCtx;

static void gen_ref(CodegenCtx *ctx, MirVarRef ref) {
    MirCtx *mir    = ctx->mir;
    StrBuffer *out = &ctx->strbuf;

    switch (ref.kind) {
    case MirVarRefKind_Const: {
        MirConst *const_ = &mir->consts[ref.idx];
        strbuf_printf(out, "%ld", const_->int_);
        break;
    }

    case MirVarRefKind_Glob: {
        MirGlob *glob = &mir->globals[ref.idx];
        strbuf_printf(out, "%.*s", str_format(glob->name));
    }

    default:
        chn_warn("unhandled ref %d", ref.kind);
        break;
    }

}

static void gen_instr(CodegenCtx *ctx, MirVarRef instr_ref) {
    MirCtx *mir    = ctx->mir;
    StrBuffer *out = &ctx->strbuf;

    MirInstr *instr = &mir->instrs[instr_ref.idx];

    strbuf_printf(out, "    ");

    switch (instr->kind) {
    case MirKind_Call: {
        dynarray_foreach(instr->fn_call.args, i) {
            MirFnArg *arg = &instr->fn_call.args[i];

            strbuf_printf(out, "mov r%zu, ", i);
            gen_ref(ctx, arg->val);
            strbuf_printf(out, "\n    ");
        }

        strbuf_printf(out, "call %.*s", str_format(instr->fn_call.name));

        break;
    }

    case MirKind_Jmp:
        strbuf_printf(out, "rjmp b%d", instr->args[0].idx);
        break;

    case MirKind_Ret:
        strbuf_printf(out, "ret");
        break;

    default:
        chn_warn("unhandled instruction %d", instr->kind);
        break;
    }

    strbuf_printf(out, "\n");
}

static void gen_block(CodegenCtx *ctx, MirVarRef block_ref) {
    MirCtx *mir    = ctx->mir;
    StrBuffer *out = &ctx->strbuf;

    MirBlock *block = &mir->blocks[block_ref.idx];

    strbuf_printf(out, "b%d:\n", block_ref.idx);

    dynarray_foreach(block->instrs, i) {
        MirVarRef instr_ref = block->instrs[i];
        gen_instr(ctx, instr_ref);
    }

    gen_instr(ctx, block->termi);
}

static void gen_fn(CodegenCtx *ctx, MirFn *fn) {
    MirCtx *mir    = ctx->mir;
    StrBuffer *out = &ctx->strbuf;

    strbuf_printf(out, "%.*s:\n", str_format(fn->name));

    dynarray_foreach(fn->blocks, i) {
        MirVarRef block_ref = fn->blocks[i];
        gen_block(ctx, block_ref);
    }

    strbuf_printf(out, "\n");
}

static void gen(CodegenCtx *ctx) {
    MirCtx *mir    = ctx->mir;
    StrBuffer *out = &ctx->strbuf;

    strbuf_printf(out, "    rjmp start__start\n\n");

    dynarray_foreach(mir->fns, i) {
        MirFn *fn = &mir->fns[i];
        gen_fn(ctx, fn);
    }

    dynarray_foreach(mir->globals, i) {
        MirGlob *glob = &mir->globals[i];

        strbuf_printf(out, "%.*s: ", str_format(glob->name));
        
        if (glob->is_string_lit) {
            strbuf_printf(out, "data.str \"%.*s\" data.8 0\n", glob->size, (char*)glob->data);
        }
    }

    strbuf_printf(out, "#include \"../../../extern/fox32/fox32rom/fox32rom.def\"\n");
    strbuf_printf(out, "#include \"../../../extern/fox32/fox32os/fox32os.def\"\n");
}

String generate_fox32(Compiler *compiler) {
    CodegenCtx ctx;
    ctx.compiler = compiler;
    ctx.mir      = &compiler->mir;
    ctx.strbuf   = strbuf_init();
    ctx.tmp_reg  = 8;
    ctx.strings  = map_init();

    gen(&ctx);

    return strbuf_to_string(&ctx.strbuf);
}
