#include "c_codegen.h"
/*
#include "ast.h"
#include "parse/parser.h"

#include <chnlib/strbuffer.h>
#include <chnlib/str.h>
#include <chnlib/logger.h>
#include <chnlib/map.h>
#include <chnlib/os.h>

typedef struct CodegenContext {
    StrBuffer strbuf;
    Compiler* compiler;
    usize indent_level;
} CodegenContext;


static void write_indent(CodegenContext* context) {
    for (usize i = 0; i < context->indent_level; i++) {
        strbuf_print_lit(&context->strbuf, "    ");
    }
}

static void write_newline(CodegenContext* context) {
    strbuf_print_lit(&context->strbuf, "\n");
    write_indent(context);
}

// TODO: this feels hacky
static String new_tmp_var(void) {
    static usize tmp_var_counter = 0;

    StrBuffer buf = strbuf_init();
    strbuf_printf(&buf, "__sil__tmp_var_%zu", tmp_var_counter++);

    return strbuf_to_string(&buf);
}

static void generate_stmt(CodegenContext* context, Stmt* stmt);
static void generate_expr(CodegenContext* context, Expr* expr);
static void generate_expr_with_block(CodegenContext* context, Expr* expr, String* bind);

static void generate_type(CodegenContext* context, type_id type) {
    TypeEntry* type_entry = &context->compiler->type_table.types[type];
    switch (type_entry->kind) {
        case TypeEntryKind_Invalid: { chn_error("generating invalid type"); }
        case TypeEntryKind_Void: { strbuf_print_lit(&context->strbuf, "void"); break; }
        case TypeEntryKind_Never: { strbuf_print_lit(&context->strbuf, "void"); break; }
        case TypeEntryKind_Ptr: {
            generate_type(context, type_entry->ptr.to);
            strbuf_printf(&context->strbuf, "%s*", type_entry->ptr.is_mut ? "" : " const");
            break;
        }
        case TypeEntryKind_Bool: { strbuf_print_lit(&context->strbuf, "bool"); break; }
        case TypeEntryKind_Int: {
            strbuf_printf(&context->strbuf, "%c%zu", type_entry->integral.is_signed ? 'i' : 'u', type_entry->bits);
            break;
        }
    }
}

static void generate_block(CodegenContext* context, Expr* block_expr, String* bind) {
    Block* block = &block_expr->block;

    strbuf_print_lit(&context->strbuf, "{\n");
    context->indent_level += 1;

    for (usize i = 0; i + 1 < dynarray_len(block->stmts); i++) {
        Stmt* stmt = &block->stmts[i];
        write_indent(context);
        generate_stmt(context, stmt);
    }

    if (dynarray_len(block->stmts) > 0) {
        Stmt* last_stmt = &block->stmts[dynarray_len(block->stmts) - 1];
        write_indent(context);

        if (last_stmt->expr->codegen.type == context->compiler->primitives.entry_void) {
            generate_stmt(context, last_stmt);

        } else if (last_stmt->kind == StmtKind_NakedExpr) {
            // naked exprs don't have blocks
            if (bind == null) {
                strbuf_print_lit(&context->strbuf, "return ");
                generate_expr(context, last_stmt->expr);
                strbuf_print_lit(&context->strbuf, ";\n");
            } else {
                strbuf_printf(&context->strbuf, "%.*s = ", str_format((*bind)));
                generate_expr(context, last_stmt->expr);
                strbuf_print_lit(&context->strbuf, ";\n");
            }

        } else if (should_remove_stmt_semi(last_stmt->expr)) {
            // block expr
            String tmp_eval = new_tmp_var();
            generate_type(context, block_expr->codegen.type);
            strbuf_printf(&context->strbuf, " %.*s;", str_format(tmp_eval));
            write_newline(context);
            generate_expr_with_block(context, last_stmt->expr, &tmp_eval);
            write_newline(context);

            if (bind != null) {
                strbuf_printf(&context->strbuf, "%.*s = %.*s;\n", str_format((*bind)), str_format(tmp_eval));
            } else {
                strbuf_printf(&context->strbuf, "return %.*s;\n", str_format(tmp_eval));
            }

            str_deinit(tmp_eval);

        } else {
            generate_stmt(context, last_stmt);
        }
    }

    context->indent_level -= 1;
    write_indent(context);
    strbuf_print_lit(&context->strbuf, "}");
}

static void generate_number_literal(CodegenContext* context, NumberLit* number_literal) {
    strbuf_print_str(&context->strbuf, number_literal->span);
}

static void generate_binop(CodegenContext* context, BinOp* binop) {
    switch (binop->kind) {
        case BinOpKind_CmpEq: strbuf_print_lit(&context->strbuf, "eqi32("); break;
        case BinOpKind_CmpNotEq: strbuf_print_lit(&context->strbuf, "neqi32("); break;
        case BinOpKind_CmpGt: strbuf_print_lit(&context->strbuf, "gti32("); break;
        case BinOpKind_CmpLt: strbuf_print_lit(&context->strbuf, "lti32("); break;
        case BinOpKind_And: strbuf_print_lit(&context->strbuf, "and("); break;
        case BinOpKind_Or: strbuf_print_lit(&context->strbuf, "or("); break;
        case BinOpKind_Assign: strbuf_print_lit(&context->strbuf, "wri32(&"); break;
        case BinOpKind_Add: strbuf_print_lit(&context->strbuf, "addi32("); break;
        case BinOpKind_Sub: strbuf_print_lit(&context->strbuf, "subi32("); break;
        case BinOpKind_Mul: strbuf_print_lit(&context->strbuf, "muli32("); break;
        case BinOpKind_Div: strbuf_print_lit(&context->strbuf, "divi32("); break;
        default: chn_error("Codegen error: Unhandled binary operator %d", binop->kind);
    }

    generate_expr(context, binop->left);
    strbuf_print_lit(&context->strbuf, ", ");
    generate_expr(context, binop->right);
    strbuf_print_lit(&context->strbuf, ")");
}

static void generate_asm(CodegenContext* context, Asm* asm, String* output) {
    String tmp_output = new_tmp_var();
    for (usize i = 0; i < dynarray_len(asm->outputs); i += 1) {
        strbuf_printf(&context->strbuf, "i64 %.*s;\n", str_format(tmp_output));
    }

    write_indent(context);
    strbuf_print_lit(&context->strbuf, "__asm__ volatile (");
    for (usize i = 0; i < dynarray_len(asm->source); i += 1) {
        strbuf_print_str(&context->strbuf, asm->source[i].span);
    }

    strbuf_print_lit(&context->strbuf, ":");

    // HACK: only one output rn
    strbuf_printf(&context->strbuf, "\"=%.*s\"(%.*s):", str_format(asm->outputs[0]), str_format(tmp_output));

    for (usize i = 0; i < dynarray_len(asm->inputs); i += 1) {
        if (i > 0) { strbuf_print_lit(&context->strbuf, ","); }

        strbuf_printf(&context->strbuf, "\"%.*s\"(", str_format(asm->inputs[i].reg));
        generate_expr(context, asm->inputs[i].val);
        strbuf_print_lit(&context->strbuf, ")");
    }

    strbuf_print_lit(&context->strbuf, ":");

    for (usize i = 0; i < dynarray_len(asm->clobbers); i += 1) {
        if (i > 0) { strbuf_print_lit(&context->strbuf, ","); }
        strbuf_printf(&context->strbuf, "\"%.*s\"", str_format(asm->clobbers[i]));
    }

    strbuf_print_lit(&context->strbuf, ");");

    if (output != null) {
        strbuf_print_lit(&context->strbuf, "\n");
        write_indent(context);
        strbuf_printf(&context->strbuf, "%.*s = %.*s;", str_format((*output)), str_format(tmp_output));
    }
}

static void generate_scope_res(CodegenContext* context, ModPath* mod_path) {
    // mod resolution not generated
    //for (usize i = 0; i < dynarray_len(mod_path->path) - 1; i += 1) {
    //    strbuf_printf(&context->strbuf, "%.*s_", str_format(mod_path->path[i]));
    //}

    strbuf_printf(
        &context->strbuf,
        "%.*s",
        str_format(mod_path->path[dynarray_len(mod_path->path) - 1])
    );
}

static void generate_expr_with_block(CodegenContext* context, Expr* expr, String* bind) {
    switch (expr->kind) {
        case ExprKind_NumberLit: {
            generate_number_literal(context, &expr->number_literal);
            break;
        }

        case ExprKind_StringLit: {
            strbuf_print_str(&context->strbuf, expr->string_literal.span);
            break;
        }

        case ExprKind_BoolLit: {
            strbuf_printf(&context->strbuf, "%s", expr->boolean ? "true" : "false");
            break;
        }

        case ExprKind_ModPath: {
            generate_scope_res(context, &expr->mod_path);
            break;
        }

        case ExprKind_FnCall: {
            FnCall* call = &expr->fn_call;

            generate_scope_res(context, &call->mod_path);

            strbuf_print_lit(&context->strbuf, "(");

            for (usize i = 0; i < dynarray_len(call->arguments); i++) {
                if (i > 0) { strbuf_print_lit(&context->strbuf, ","); }

                Expr* arg = call->arguments[i];
                generate_expr(context, arg);
            }

            strbuf_print_lit(&context->strbuf, ")");
            
            break;
        }

        case ExprKind_Let: {
            generate_type(context, expr->codegen.type);

            if (should_remove_stmt_semi(expr->let.value)) {
                strbuf_printf(&context->strbuf, " %.*s;\n", str_format(expr->let.name));
                write_indent(context);
                generate_expr_with_block(context, expr->let.value, &expr->let.name);

                break;
            }

            strbuf_printf(&context->strbuf, " %.*s = ", str_format(expr->let.name));
            generate_expr(context, expr->let.value);

            break;
        }

        case ExprKind_Ret: {
            strbuf_print_lit(&context->strbuf, "return ");
            generate_expr(context, expr->ret.expr);
            
            break;
        }

        case ExprKind_Block: {
            generate_block(context, expr, bind);
            break;
        }

        case ExprKind_BinOp: {
            generate_binop(context, &expr->bin_op);
            break;
        }

        case ExprKind_UnOp: {
            switch (expr->unary_op.kind) {
                case UnOpKind_Neg:
                    strbuf_print_lit(&context->strbuf, "-(");
                    break;
                case UnOpKind_AddrOf:
                    strbuf_print_lit(&context->strbuf, "&(");
                    break;
            }
            generate_expr(context, expr->unary_op.expr);
            strbuf_print_lit(&context->strbuf, ")");
            break;
        }

        case ExprKind_If: {
            strbuf_print_lit(&context->strbuf, "if (");
            generate_expr(context, expr->if_expr.condition);
            strbuf_print_lit(&context->strbuf, ") ");
            generate_expr_with_block(context, expr->if_expr.then, bind);
            if (expr->if_expr.otherwise != null) {
                strbuf_print_lit(&context->strbuf, " else ");
                generate_expr_with_block(context, expr->if_expr.otherwise, bind);
            }

            break;
        }

        case ExprKind_Match: {
            Match* match = &expr->match;
            strbuf_print_lit(&context->strbuf, "switch (");
            generate_expr_with_block(context, expr->match.condition, bind);
            strbuf_print_lit(&context->strbuf, ") {\n");
            context->indent_level += 1;
            for (usize i = 0; i < dynarray_len(match->arms); i++) {
                MatchArm* arm = &match->arms[i];
                write_indent(context);
                strbuf_print_lit(&context->strbuf, "case ");
                generate_number_literal(context, &arm->pattern);
                strbuf_print_lit(&context->strbuf, ": ");
                generate_expr_with_block(context, arm->then, bind);
                if (arm->then->kind != ExprKind_Block) {
                    strbuf_print_lit(&context->strbuf, "; break;");
                } else {
                    strbuf_print_lit(&context->strbuf, " break;");
                }
                strbuf_print_lit(&context->strbuf, "\n");
            }
            context->indent_level -= 1;
            write_indent(context);
            strbuf_print_lit(&context->strbuf, "}");
            break;
        }

        case ExprKind_Loop: {
            strbuf_print_lit(&context->strbuf, "while (true) ");
            generate_expr_with_block(context, expr->loop.body, bind);
            break;
        }

        case ExprKind_Break: { strbuf_print_lit(&context->strbuf, "break"); break; }
        case ExprKind_Continue: { strbuf_print_lit(&context->strbuf, "continue"); break; }
        case ExprKind_Unreachable: { strbuf_print_lit(&context->strbuf, ""); break; }

        case ExprKind_Asm: {
            generate_asm(context, &expr->asm, bind);
            break;
        }

        case ExprKind_Cast: {
            strbuf_print_lit(&context->strbuf, "(");
            generate_type(context, expr->codegen.type);
            strbuf_print_lit(&context->strbuf, ")");
            generate_expr(context, expr->cast.expr);
            break;
        }

        default: chn_error("codegen error: unhandled expr %d", expr->kind);
    }
}

static void generate_expr(CodegenContext* context, Expr* expr) {
    generate_expr_with_block(context, expr, null);
}

static void generate_stmt(CodegenContext* context, Stmt* stmt) {
    switch (stmt->kind) {
        case StmtKind_Expr:
        case StmtKind_NakedExpr:
            generate_expr(context, stmt->expr);
            strbuf_printf(&context->strbuf, "%s\n", should_remove_stmt_semi(stmt->expr) ? "" : ";");
    }
}

static void generate_fn_signature(CodegenContext* context, Item* item) {
    FnSig* signature;
    if (item->kind == ItemKind_FnDef) {
        signature = &item->fn_definition.signature;
    } else if (item->kind == ItemKind_ExternFn) {
        signature = &item->extern_fn.signature;
    } else {
        chn_error("Cannot generate signature for item type %d", item->kind);
    }

    generate_type(context, signature->return_type->codegen.type);

    strbuf_printf(&context->strbuf, " %.*s(", str_format(item->name));

    // void as empty parameters
    if (dynarray_len(signature->parameters) == 0) {
        strbuf_print_lit(&context->strbuf, "void");
    }
    for (usize i = 0; i < dynarray_len(signature->parameters); i++) {
        if (i > 0) { strbuf_print_lit(&context->strbuf, ", "); }
        FnParam* parameter = &signature->parameters[i];
        generate_type(context, parameter->type->codegen.type);
        strbuf_printf(&context->strbuf, " const %.*s", str_format(parameter->name));
    }

    strbuf_print_lit(&context->strbuf, ")");
}

static void generate_definition(CodegenContext* context, Item* item) {
    switch (item->kind) {
        case ItemKind_FnDef:
            if (!item->visibility.is_pub) {
                strbuf_print_lit(&context->strbuf, "static ");
            }
            generate_fn_signature(context, item);
            strbuf_print_lit(&context->strbuf, " ");
            generate_block(context, item->fn_definition.body, null);
            strbuf_print_lit(&context->strbuf, "\n\n");
            break;
        
        default: return;
    }
}

static void generate_forward_declarations(CodegenContext* context, Module* module) {
    // fn
    {
        for (usize i = 0; i < dynarray_len(module->ast->items); i += 1) {
            Item* item = &module->ast->items[i];

            if (item->kind != ItemKind_FnDef and item->kind != ItemKind_ExternFn) { continue; }

            if (item->kind != ItemKind_ExternFn and !item->visibility.is_pub) {
                strbuf_print_lit(&context->strbuf, "static ");
            }
            generate_fn_signature(context, item);
            strbuf_print_lit(&context->strbuf, ";\n");
        };
        strbuf_print_lit(&context->strbuf, "\n");
    }

    // consts
    {
        for (usize i = 0; i < dynarray_len(module->ast->items); i += 1) {
            Item* item = &module->ast->items[i];

            if (item->kind != ItemKind_Const) { continue; }

            strbuf_printf(&context->strbuf, "#define %.*s (", str_format(item->name));
            generate_expr(context, item->constant.value);
            strbuf_print_lit(&context->strbuf, ")\n");
        };
    }
}

String c_codegen_generate(Compiler* compiler) {
    CodegenContext context;
    context.indent_level = 0;
    context.compiler = compiler;
    context.strbuf = strbuf_init();

    // prelude
    Maybe(String) maybe_prelude = os_read_file(str_from_lit("prelude.c"));
    if (maybe_prelude == None) {
        chn_error(":)");
    }
    String prelude = unwrap(maybe_prelude);
    strbuf_print_str(&context.strbuf, prelude);

    // forward decl
    {
        MapIter iter = map_iter(compiler->modules);
        while (map_next(context.compiler->modules, iter)) {
            Module* import_module = map_iter_val(compiler->modules, iter);

            generate_forward_declarations(&context, import_module);
        }
    }

    // definitions
    {
        MapIter iter = map_iter(compiler->modules);
        while (map_next(context.compiler->modules, iter)) {
            Module* import_module = map_iter_val(compiler->modules, iter);

            for (usize i = 0; i < dynarray_len(import_module->ast->items); i++) {
                Item* item = &import_module->ast->items[i];
                generate_definition(&context, item);
            }
        }
    }

    return strbuf_to_string(&context.strbuf);
}
*/
