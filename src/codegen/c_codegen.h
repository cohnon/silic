#ifndef C_CODEGEN_H
#define C_CODEGEN_H

#include "compiler.h"
#include "module.h"

#include <chnlib/str.h>


String c_codegen_generate(Compiler *module);

#endif
