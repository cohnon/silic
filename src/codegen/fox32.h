#ifndef CODEGEN_FOX32_H
#define CODEGEN_FOX32_H

#include "compiler.h"

#include <chnlib/strbuffer.h>


String generate_fox32(Compiler *compiler);

#endif
