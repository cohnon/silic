#include "error_msg.h"

#include <chnlib/logger.h>
#include "stdarg.h"
#include "string.h"
#include "stdio.h"


#define RESET "\033[0m"
#define ERROR_RED "\033[1;31m"
#define OTHER_BLUE "\033[34m"
#define FADED "\033[90m"

errmsg_id errors_new(DynArray(ErrorMsg) *errors, String path, TextPosition pos, String span, const char *msg, ...) {
    // format message
    usize max_length = strlen(msg) + 256;
    char *formatted_msg = malloc(max_length);

    va_list args;
    va_start(args, msg);
    vsnprintf(formatted_msg, max_length, msg, args);
    va_end(args);

    // add error
    ErrorMsg error = { path, pos.line, pos.column, span, formatted_msg, null };
    dynarray_push(*errors, &error);

    return dynarray_len(*errors) - 1;
}

void errors_add_hint(DynArray(ErrorMsg) errors, errmsg_id id, const char *hint, ...) {
    // format message
    usize max_length = strlen(hint) + 256;
    char *formatted_msg = malloc(max_length);

    va_list args;
    va_start(args, hint);
    vsnprintf(formatted_msg, max_length, hint, args);
    va_end(args);

    ErrorMsg *error = &errors[id];
    error->hint = formatted_msg;
}

void errors_display(DynArray(ErrorMsg) errors) {
    for (usize i = 0; i < dynarray_len(errors); i += 1) {
        ErrorMsg *error = &errors[i];
        fprintf(stderr, ERROR_RED "error:" RESET " %s\n", error->message);

        // TODO: absolutely hideous
        int line_num_width = 0;
        for (int i = error->line; i > 0; i /= 10) { line_num_width += 1; }
        // print border
        fprintf(stderr, FADED "    ╭─[" RESET "%.*s:%d:%d" FADED "]───\n", str_format(error->path), error->line, error->col);
        fprintf(stderr, "%.*s%.*d │ " RESET, 3 - line_num_width, "  ", line_num_width, error->line);
        // print source
        char *start = (char*)error->span.ptr - (error->col - 1);
        while (*start != '\n' && *start != 0) {
            if (start == error->span.ptr) { fprintf(stderr, ERROR_RED); }
            putc(*start, stderr);
            if (start == error->span.ptr+ error->span.len - 1) { fprintf(stderr, RESET); }
            start += 1;
        }

        // ^^^ underlining
        fprintf(stderr, FADED "\n    │ ");
        for (usize i = 0; i < error->col - 1; i++) { putc(' ', stderr); }
        fprintf(stderr, ERROR_RED);
        for (usize i = 0; i < error->span.len; i += 1) { fprintf(stderr, "^"); }

        // hint
        if (error->hint != null) {
            fprintf(stderr, " %s", error->hint);
        }
        fprintf(stderr, FADED "\n    ╰─────\n" RESET);
    }
}
