#ifndef COMPILER_H
#define COMPILER_H

#include "error_msg.h"
#include "module.h"
#include "ast/namespace.h"

#include <chnlib/mem/arena.h>
#include <fir.h>


typedef struct Compiler {
    String root_path;

    bool build;
    bool verbose;

    Module       *start_module;
    Map(Module*) modules;

    Namespace ns;
    TypeTable type_table;

    FirModule *fir;

    DynArray(ErrorMsg) errors;

    struct {
        type_id entry_void;
        type_id entry_never;

        type_id entry_num_lit;

        type_id entry_c_char;
        type_id entry_c_str;

        type_id entry_bool;

        type_id entry_usize;
        type_id entry_isize;
        
        type_id entry_u8;
        type_id entry_u16;
        type_id entry_u32;
        type_id entry_u64;

        type_id entry_i8;
        type_id entry_i16;
        type_id entry_i32;
        type_id entry_i64;
    } primitives;
} Compiler;


void compiler_init(Compiler *compiler, bool build, bool verbose);
void compiler_deinit(Compiler *compiler);

bool compiler_compile(Compiler *compiler, String path);

#endif
