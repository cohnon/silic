#include "compiler.h"

#include "parse/lexer.h"
#include "parse/parser.h"
#include "ast/nameres.h"
#include "ast/typecheck.h"
#include "ast/flow.h"

#include <chnlib/map.h>
#include <chnlib/logger.h>
#include <chnlib/os.h>
#include <stdio.h>


static void setup_primitive_types(Compiler *compiler) {

    // void
    compiler->primitives.entry_void = typetbl_new_type(&compiler->type_table, TypeKind_Void, 0);

    // never
    compiler->primitives.entry_never = typetbl_new_type(&compiler->type_table, TypeKind_Never, 0);

    // num lit
    compiler->primitives.entry_num_lit = typetbl_new_type(&compiler->type_table, TypeKind_NumLit, 64);

    // char
    compiler->primitives.entry_c_char = typetbl_new_int(&compiler->type_table, 8, false);
    ns_add_type(&compiler->ns, str_from_lit("c_char"), compiler->primitives.entry_c_char);

    // bool
    compiler->primitives.entry_bool = typetbl_new_type(&compiler->type_table, TypeKind_Bool, 8);
    ns_add_type(&compiler->ns, str_from_lit("bool"), compiler->primitives.entry_bool);

    // usize
    compiler->primitives.entry_usize = typetbl_new_int(&compiler->type_table, 64, false);
    ns_add_type(&compiler->ns, str_from_lit("usize"), compiler->primitives.entry_usize);

    // isize
    compiler->primitives.entry_isize = typetbl_new_int(&compiler->type_table, 64, true);
    ns_add_type(&compiler->ns, str_from_lit("isize"), compiler->primitives.entry_isize);

    // unsigned int
    compiler->primitives.entry_u8 =  typetbl_new_int(&compiler->type_table, 8,  false);
    compiler->primitives.entry_u16 = typetbl_new_int(&compiler->type_table, 16, false);
    compiler->primitives.entry_u32 = typetbl_new_int(&compiler->type_table, 32, false);
    compiler->primitives.entry_u64 = typetbl_new_int(&compiler->type_table, 64, false);

    ns_add_type(&compiler->ns, str_from_lit("u8"),  compiler->primitives.entry_u8);
    ns_add_type(&compiler->ns, str_from_lit("u16"), compiler->primitives.entry_u16);
    ns_add_type(&compiler->ns, str_from_lit("u32"), compiler->primitives.entry_u32);
    ns_add_type(&compiler->ns, str_from_lit("u64"), compiler->primitives.entry_u64);

    // c str
    compiler->primitives.entry_c_str = typetbl_new_ptr(
        &compiler->type_table,
        compiler->primitives.entry_u8,
        false
    );

    // signed int
    compiler->primitives.entry_i8 = typetbl_new_int(&compiler->type_table, 8, true);
    compiler->primitives.entry_i16 = typetbl_new_int(&compiler->type_table, 16, true);
    compiler->primitives.entry_i32 = typetbl_new_int(&compiler->type_table, 32, true);
    compiler->primitives.entry_i64 = typetbl_new_int(&compiler->type_table, 64, true);

    ns_add_type(&compiler->ns, str_from_lit("i8"),  compiler->primitives.entry_i8);
    ns_add_type(&compiler->ns, str_from_lit("i16"), compiler->primitives.entry_i16);
    ns_add_type(&compiler->ns, str_from_lit("i32"), compiler->primitives.entry_i32);
    ns_add_type(&compiler->ns, str_from_lit("i64"), compiler->primitives.entry_i64);
}

void compiler_init(Compiler *compiler, bool build, bool verbose) {
    compiler->build = build;
    compiler->verbose = verbose;

    compiler->modules = map_init();

    ns_init(&compiler->ns, null);
    typetbl_init(&compiler->type_table);

    compiler->errors = dynarray_init();

    setup_primitive_types(compiler);
}

void compiler_deinit(Compiler *compiler) {
    map_deinit(compiler->modules);
    typetbl_deinit(&compiler->type_table);
    dynarray_deinit(compiler->errors);
}

static Module* add_module(Compiler *compiler, String proj_path) {
    // TODO: make this modifiable
    bool found = false;
    String abs_path;
    String lookup_paths[2] = { compiler->root_path, str_from_lit("std") };
    for (usize lookup_idx = 0; lookup_idx < 2; lookup_idx += 1) {
        abs_path = str_copy(lookup_paths[lookup_idx]);
        os_path_join(&abs_path, proj_path);
        str_append(&abs_path, str_from_lit(".sil"));

        if (os_path_exists(abs_path)) {
            found = true;
            break;

        } else {
            str_deinit(abs_path);
        }
    }

    if (not found) { chn_error("file not found %.*s", str_format(abs_path)); }

    String src;
    os_read_file(&src, abs_path);


    Module *module = malloc(sizeof(Module));
    module_init(module, proj_path, src);

    // lex file
    chn_info("lexing...");

    lex_module(module);

    if (dynarray_len(module->errors) > 0) {
        errors_display(module->errors);
        return null;
    }

    // print tokens
    if (compiler->verbose) {
        printf("Tokens\n------\n");
        for (usize i = 0; i < dynarray_len(module->token_list); i++) {
            Token *token = &module->token_list[i];
            printf("%s: ", token_string(token->kind));
            token_print(token);
            printf("\n");
        }

    }
    chn_info("lexed");

    // parse file
    chn_info("parsing...");

    parse_module(module);

    if (dynarray_len(module->errors) > 0) {
        errors_display(module->errors);
        return null;
    }

    chn_info("parsed");


    // --------- //
    // top level //
    DynArray(ModImport) imports = module->imports;
    for (usize import_idx = 0; import_idx < dynarray_len(imports); import_idx += 1) {
        ModImport *import = &imports[import_idx];

        String proj_path = str_copy(import->use->path[0]);
        for (usize path_idx = 1; path_idx < dynarray_len(import->use->path); path_idx += 1) {
            os_path_join(&proj_path, import->use->path[path_idx]);
        }

        Module *imported_module = null;
        Module** existing_module = map_get_ref(compiler->modules, proj_path);
        if (existing_module != null) {
            imported_module = *existing_module;
        } else {
            imported_module = try(add_module(compiler, proj_path));
        }

        imported_module->name = str_copy(dynarray_last(import->use->path));
        import->module = imported_module;
        import->use->ns = &imported_module->ns;
    }

    map_insert(compiler->modules, proj_path, &module);

    return module;
}

bool compiler_compile(Compiler *compiler, String filepath) {
    String root_dir = os_path_dir(filepath);
    compiler->root_path = root_dir;

    if (compiler->verbose) {
        chn_log_level(Chn_LogLevel_Debug);
    } else {
        chn_log_level(Chn_LogLevel_Error);
    }

    compiler->start_module = try(add_module(compiler, str_from_lit("main")));
    compiler->start_module->name = str_from_lit("root");

    bool has_main_fn = false;
    for (usize i = 0; i < dynarray_len(compiler->start_module->ast->items); i += 1) {
        Item *item = compiler->start_module->ast->items[i];
        if (item->kind == ItemKind_FnDef
        and str_eq(item->name, str_from_lit("main"))
        and item->visibility.is_pub) {
            has_main_fn = true;
        }
    }

    if (has_main_fn) {
        Module* start_module = try(add_module(compiler, str_from_lit("start")));
        start_module->name = str_from_lit("start");

        compiler->start_module = start_module;
    }

    // -------- //
    // analysis //
    chn_info("analyzing...");

    ir_nameres(compiler);
    ir_typecheck(compiler);
    ir_flow(compiler);

    if (dynarray_len(compiler->errors) > 0) {
        errors_display(compiler->errors);
        return false;
    }

    chn_info("analyzed");


    // --- //
    // mir //
    /*
    chn_info("lowering...");

    mir_init(&compiler->mir);
    mir_lower(compiler);

    chn_info("lowered");

    mir_ssa_pass(&compiler->mir);

    if (compiler->verbose) {
        mir_debug_print(&compiler->mir);
    }
    */


    // ------- //
    // codegen //
    /*chn_info("generating ir...");

    String ir = generate_fox32(compiler);
    chn_debug("ir length: %zu", ir.len);
    os_create_file(str_from_lit("build/ir.asm"), ir);

    if (compiler->verbose) {
        printf("%.*s\n", str_format(ir));
    }

    chn_info("generated");
    */

    return true;
}

