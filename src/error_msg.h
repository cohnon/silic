#ifndef ERROR_MSG_H
#define ERROR_MSG_H

#include "parse/token.h"
#include <chnlib/dynarray.h>


typedef u32 errmsg_id;

typedef struct ErrorMsg {
    String path;
    u32 line;
    u32 col;
    String span;
    const char *message;
    const char *hint;
} ErrorMsg;

__attribute__((format(printf, 5, 6)))
errmsg_id errors_new(DynArray(ErrorMsg) *errors, String path, TextPosition pos, String span, const char *msg, ...);

__attribute__((format(printf, 3, 4)))
void errors_add_hint(DynArray(ErrorMsg) errors, errmsg_id id, const char *hint, ...);

void errors_display(DynArray(ErrorMsg) errors);


#endif
