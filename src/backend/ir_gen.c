#include "ir_gen.h"

#include <fir.h>
#include <chnlib/dynarray.h>
#include <chnlib/logger.h>
#include <chnlib/map.h>
#include <assert.h>


// typedef struct LowerCtx {
//     Compiler *compiler;
//     Module *cur_module;
//     Namespace *cur_ns;
//     FirModule *fir;
//     FirBuilder *firb;
// } LowerCtx;
// 
// static void enter_scope(LowerCtx *ctx, Namespace *ns) {
//     ctx->cur_ns = ns;
// }
// 
// static void exit_scope(LowerCtx *ctx) {
//     assert(ctx->cur_ns->parent != null and "HI");
//     ctx->cur_ns = ctx->cur_ns->parent;
// }
// 
// static FirVal lower_expr(LowerCtx *ctx, Expr *expr);
// static void lower_stmt(LowerCtx *ctx, Stmt *stmt);
// 
// static FirType convert_type(LowerCtx *ctx, type_id id) {
//     TypeEntry *entry = typetbl_get(&ctx->compiler->type_table, id);
//     switch (entry->kind) {
//     case TypeKind_Invalid:
//     case TypeKind_Void: return fir_ty_void();
//     case TypeKind_Ptr: return fir_ty_ptr();
//     case TypeKind_NumLit:
//     case TypeKind_Int: return fir_ty_int(entry->bits);
//     default: chn_warn("unhandled type %d", entry->kind); return fir_ty_void();
//     }
// }
// 
// static String mangle_name(LowerCtx *ctx, Item *item) {
//     if (item->kind == ItemKind_ExternFn) {
//         return item->name;
//     }
// 
//     String name = item->name;
// 
//     String mod_path = ctx->cur_module->path;
// 
//     // 'path/to/module' + '_' + 'item_name'
//     // 'path_to_module_item_name'
//     String mangled_name = str_init(mod_path.len + name.len + 1);
// 
//     for (usize i = 0; i < mod_path.len; i += 1) {
//         mangled_name.ptr[i] =
//             mod_path.ptr[i] == '/'
//             ? '_'
//             : mod_path.ptr[i];
//     }
// 
//     mangled_name.ptr[mod_path.len] = '_';
// 
//     for (usize i = 0; i < name.len; i += 1) {
//         mangled_name.ptr[i + mod_path.len + 1] = name.ptr[i];
//     }
// 
//     return mangled_name;
// }
// static FirVal lower_block(LowerCtx *ctx, Expr *expr, bool implicit_return) {
//     assert(expr->kind == ExprKind_Block);
// 
//     Block *block = &expr->block;
// 
//     if (dynarray_len(block->stmts) == 0) {
//         return fir_val_invalid();
//     }
// 
//     enter_scope(ctx, &block->ns);
// 
//     for (usize i = 0; i < dynarray_len(block->stmts) - 1; i += 1) {
//         lower_stmt(ctx, block->stmts[i]);
//     }
// 
// 
//     FirVal ref = fir_val_invalid();
// 
//     Stmt *last_stmt = dynarray_last(block->stmts);
//     if (last_stmt->kind == StmtKind_NakedExpr) {
//         ref = lower_expr(ctx, last_stmt->expr);
//     } else {
//         lower_stmt(ctx, last_stmt);
//     }
// 
//     if (implicit_return) {
//         bool last_stmt_is_expr = 
//             last_stmt->kind == StmtKind_Expr or last_stmt->kind == StmtKind_NakedExpr;
// 
//         chn_debug("%d %d %d", last_stmt_is_expr, last_stmt->expr->kind != ExprKind_Ret, last_stmt->expr->codegen.type == TypeKind_Void);
// 
//         if (last_stmt_is_expr and last_stmt->expr->kind != ExprKind_Ret) {
//             if (last_stmt->expr->codegen.type == TypeKind_Void) {
//                 firb_ret_void(ctx->firb);
//             } else if (last_stmt->expr->codegen.type == TypeKind_Never) {
//                 // TODO: should be unreachable
//                 firb_ret(ctx->firb, ref);
//             } else {
//                 firb_ret(ctx->firb, ref);
//             }
//         }
//     }
// 
//     exit_scope(ctx);
// 
//     return ref;
// }
// 
// static void lower_item(LowerCtx *ctx, Item *item) {
//     switch (item->kind) {
//         case ItemKind_Use:
//         case ItemKind_Const:
//         case ItemKind_ExternFn: {
//             break;
//         }
// 
//         case ItemKind_FnDef: {
//             NsEntry *entry = ns_local(ctx->cur_ns, item->name);
//             assert(entry != null and "invalid ast");
// 
//             // enter parameter scope
//             enter_scope(ctx, &item->fn_definition.ns);
// 
//             FnDef *fn_def = &item->fn_definition;
//             FnSig *fn_sig = &fn_def->signature;
// 
//             TypeEntry *fn_type = typetbl_get(&ctx->compiler->type_table, entry->type.id);
// 
//             size_t n_params = dynarray_len(fn_sig->parameters);
//             FirType *param_types = malloc(sizeof(FirType) * n_params);
//             for (size_t i = 0; i < n_params; i += 1) {
//                 param_types[i] = convert_type(ctx, fn_type->fn.param_types[i]);
//             }
// 
//             entry->fir_func = fir_func_create(ctx->fir,
//                 fir_sym_slc(entry->mangled_name.ptr, entry->mangled_name.len),
//                 convert_type(ctx, fn_type->fn.ret_type),
//                 param_types,
//                 n_params
//             );
// 
//             lower_block(ctx, item->fn_definition.body, true);
// 
//             exit_scope(ctx);
// 
//             break;
//         }
//     }
// }
// 
// static void lower_stmt(LowerCtx *ctx, Stmt *stmt) {
//     switch (stmt->kind) {
//         case StmtKind_Item: {
//             break;
//         }
//         
//         case StmtKind_NakedExpr:
//         case StmtKind_Expr: {
//             lower_expr(ctx, stmt->expr);
//             break;
//         }
//     }
// }
// 
// void ir_gen(Compiler *compiler) {
//     LowerCtx ctx;
//     ctx.compiler = compiler;
//     ctx.fir = fir_mod_create();
//     ctx.firb = fir_mod_get_builder(ctx.fir);
// 
//     map_foreach(compiler->modules, module) {
//         ctx.cur_module = module;
//         enter_scope(&ctx, &module->ns);
// 
//         dynarray_foreach(module->ast->items, i) {
//             Item *item = module->ast->items[i];
//             NsEntry *entry = ns_local(ctx.cur_ns, item->name);
//             assert(entry != null and "invalid ast");
// 
//             entry->mangled_name = mangle_name(&ctx, item);
//         }
//     }}
// 
//     map_foreach(compiler->modules, module) {
//         ctx.cur_module = module;
//         enter_scope(&ctx, &module->ns);
// 
//         dynarray_foreach(module->ast->items, i) {
//             lower_item(&ctx, module->ast->items[i]);
//         }
//     }}
// }

