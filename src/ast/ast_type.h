#ifndef AST_TYPE_H
#define AST_TYPE_H

#include "ast/typetable.h"
#include <chnlib/chntype.h>
#include <chnlib/str.h>


// not sure where to put this
typedef struct ModPath {
    DynArray(String) path;
} ModPath;

typedef struct Ast_Type Ast_Type;
typedef enum Ast_TypeKind {
    AstTypeKind_Void,
    AstTypeKind_Never,
    AstTypeKind_Ptr,
    AstTypeKind_Symbol,
} Ast_TypeKind;

typedef struct Ast_TypePtr {
    Ast_Type *to;
    bool      is_mut;
} Ast_TypePtr;

typedef struct ModPath ModPath;
typedef struct Ast_TypePath {
    ModPath path;
} Ast_TypePath;

typedef struct Ast_Type {
    Ast_TypeKind  kind;
    type_id       type;

    union {
        Ast_TypePath symbol;
        Ast_TypePtr  ptr;
    };

} Ast_Type;

#endif
