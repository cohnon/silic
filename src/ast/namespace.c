#include "namespace.h"

#include "ast/ast.h"
#include <chnlib/dynarray.h>
#include <chnlib/maybe.h>
#include <chnlib/logger.h>
#include <assert.h>


void ns_init(Namespace *ns, Namespace *parent) {
    ns->parent = parent;
    ns->symbols = map_init();
}

#define RET_IF_EXISTS(ns) do { \
    NsEntry *existing = map_get_ref(ns->symbols, name); \
    if (existing != null) { return null; } \
} while(false)

NsEntry* ns_add(Namespace *ns, String name, NsEntryKind kind) {
    RET_IF_EXISTS(ns);

    NsEntry new = { .kind = kind };
    map_insert(ns->symbols, name, &new);

    return null;
}

NsEntry* ns_add_empty(Namespace *ns, String name) {
    RET_IF_EXISTS(ns);

    NsEntry new = { 0 };
    map_insert(ns->symbols, name, &new);

    return null;
}

NsEntry* ns_add_module(Namespace *ns, String name, Namespace *to) {
    RET_IF_EXISTS(ns);

    NsEntry new = {
        NsEntryKind_Mod,
        .mod = { to },
    };

    map_insert(ns->symbols, name, &new);

    return null;
}

NsEntry* ns_add_type(Namespace *ns, String name, type_id type) {
    RET_IF_EXISTS(ns);

    NsEntry new = {
        NsEntryKind_Type,
        .type = { type },
    };

    map_insert(ns->symbols, name, &new);

    return null;
}

NsEntry* ns_local(Namespace *ns, String name) {
    return map_get_ref(ns->symbols, name);
}

// move to nameres maybe
NsEntry* ns_resolve(Namespace *ns, ModPath *modpath) {
    assert(ns != null);
    String path_root = modpath->path[0];

    Namespace *cur = ns;

    while (ns_local(cur, path_root) == null) {
        cur = cur->parent;
        if (cur == null) { return null; }
    }

    for (usize i = 0; i < dynarray_len(modpath->path) - 1; i += 1) {
        String res = modpath->path[i];

        NsEntry *entry = try(ns_local(cur, res));
        if (entry->kind != NsEntryKind_Mod) { return null; }

        cur = entry->mod.ns;
    }

    NsEntry *entry = try(ns_local(cur, dynarray_last(modpath->path)));
    if (entry->kind == NsEntryKind_Mod) { return null; }

    return entry;
}

