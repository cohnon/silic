#ifndef AST_EXPR_H
#define AST_EXPR_H

#include "parse/token.h"
#include "ast/ast_type.h"
#include "ast/namespace.h"
#include <chnlib/str.h>
#include <chnlib/dynarray.h>


typedef struct Stmt Stmt;
typedef struct Expr Expr;

typedef enum ExprKind {
    ExprKind_StringLit,
    ExprKind_NumberLit,
    ExprKind_BoolLit,
    ExprKind_Symbol,
    ExprKind_If,
    ExprKind_Block,
    ExprKind_UnOp,
    ExprKind_BinOp,
    ExprKind_Let,
    ExprKind_Switch,
    ExprKind_Ret,
    ExprKind_FnCall,
    ExprKind_Loop,
    ExprKind_Break,
    ExprKind_Continue,
    ExprKind_Unreachable,
    ExprKind_Asm,
    ExprKind_Cast,
} ExprKind;

typedef struct StringLit {
    String span;
} StringLit;

typedef struct NumberLit {
    String span;
    union {
        u64 int_;
        f64 float_;
    };
} NumberLit;

typedef struct Symbol {
    ModPath path;
} Symbol;

typedef struct Block {
    DynArray(Stmt*) stmts;

    Namespace ns;
} Block;

typedef struct If {
    Expr *condition;
    Expr *then;
    Expr *otherwise;
} If;

typedef struct SwitchBranch {
    Expr *value;
    Expr *then;
} SwitchBranch;

typedef struct Switch {
    Expr *condition;
    DynArray(SwitchBranch) branches;
} Switch;

typedef struct Loop {
    Expr *body;
} Loop;

typedef enum BinOpKind {
    BinOpKind_And,
    BinOpKind_Or,
    BinOpKind_CmpEq,
    BinOpKind_CmpNotEq,
    BinOpKind_CmpGt,
    BinOpKind_CmpLt,
    BinOpKind_Assign,
    BinOpKind_Add,
    BinOpKind_Sub,
    BinOpKind_Mul,
    BinOpKind_Div,
} BinOpKind;

typedef struct BinOp {
    BinOpKind kind;
    Expr *left;
    Expr *right;
} BinOp;

typedef enum OpPrec {
    OpPrec_Invalid = 0,

    OpPrec_And = 5,
    OpPrec_Or = 5,

    OpPrec_CmpEq = 10,
    OpPrec_CmpNotEq = 10,

    OpPrec_CmpGt = 15,
    OpPrec_CmpLt = 15,

    OpPrec_Assign = 20,

    OpPrec_Add = 30,
    OpPrec_Sub = 30,

    OpPrec_Mul = 40,
    OpPrec_Div = 40,
} OpPrec;

typedef enum UnOpKind {
    UnOpKind_Neg,
    UnOpKind_AddrOf,
} UnOpKind;

typedef struct UnOp {
    UnOpKind kind;
    Expr *expr;
} UnOp;

typedef struct Let {
    String name;
    Ast_Type *type;
    Expr *value;
} Let;

typedef struct FnCall {
    ModPath         modpath;
    DynArray(Expr*) arguments;
} FnCall;

typedef struct AsmInput {
    String  reg;
    Expr   *val;
} AsmInput;

typedef struct Asm {
    DynArray(AsmInput)  inputs;
    DynArray(String)    clobbers;
    DynArray(String)    outputs;
    DynArray(StringLit) source;
} Asm;

typedef struct Cast {
    Expr     *expr;
    Ast_Type *to;
} Cast;

typedef struct Ret {
    Expr *expr;
} Ret;

typedef struct Expr {
    ExprKind kind;
    union {
        StringLit string_literal;
        NumberLit number_literal;
        Block block;
        If if_expr;
        Symbol symbol;
        Switch switch_expr;
        BinOp bin_op;
        UnOp unary_op;
        Let let;
        Ret ret;
        FnCall fn_call;
        bool boolean;
        Loop loop;
        Asm asm;
        Cast cast;
        ModPath modpath;
    };

    // used for error messages
    TextPosition src_pos;
    String span;

    struct {
        type_id type;
    } codegen;
} Expr;

#endif
