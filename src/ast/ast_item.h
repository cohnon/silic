#ifndef AST_ITEM_H
#define AST_ITEM_H

#include "ast_type.h"
#include "ast_expr.h"

#include <chnlib/str.h>
#include <chnlib/dynarray.h>


typedef enum ItemKind {
    ItemKind_FnDef,
    ItemKind_ExternFn,
    ItemKind_Const,
    ItemKind_Use,
} ItemKind;

typedef struct Visibilty {
    bool is_pub;
} Visibility;

typedef struct FnParam {
    String name;
    Ast_Type *type;
} FnParam;

typedef struct FnSig {
    DynArray(FnParam) parameters;
    Ast_Type *return_type;
} FnSig;

typedef struct FnDef {
    FnSig signature;
    Namespace ns;
    Expr *body;
} FnDef;

typedef struct ExternFn {
    FnSig signature;
} ExternFn;

typedef struct Constant {
    Ast_Type *type;
    Expr *value;
} Constant;

typedef struct ModuleRes {
    String name;
    struct ModuleRes *child;
} ModuleRes;

typedef struct Use {
    // use path/to/module;
    DynArray(String) path;
    // use path/to/module: items, to, extract;
    DynArray(String) items;
    // use path/to/module:*
    bool extract_all;
    // reference to module's namespace
    Namespace *ns;
} Use;

typedef struct Item {
    ItemKind kind;

    Visibility visibility;
    String name;

    union {
        FnDef fn_definition;
        ExternFn extern_fn;
        Constant constant;
        Use use;
    };

    usize id;

    struct {
        type_id type;
    } codegen;
} Item;

#endif
