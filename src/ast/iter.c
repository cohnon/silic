#include "iter.h"

#include <chnlib/logger.h>
#include <chnlib/maybe.h>


bool iter_stmt(AstIterCtx*, Stmt*);
bool iter_expr(AstIterCtx*, Expr*);

static void enter_scope(AstIterCtx *ctx, Namespace *ns) {
    if (ns->symbols == null) {
        ns_init(ns, ctx->cur_ns);
    }

    assert(ns->symbols != null);
    ctx->cur_ns = ns;
}

static void exit_scope(AstIterCtx *ctx) {
    assert(ctx->cur_ns != null);

    ctx->cur_ns = ctx->cur_ns->parent;
}

bool iter_blk(AstIterCtx *ctx, Expr *expr) {
    assert(expr->kind == ExprKind_Block);

    Block *blk = &expr->block;

    enter_scope(ctx, &blk->ns);

    for (size_t i = 0; i < dynarray_len(blk->stmts); i += 1) {
        iter_stmt(ctx, blk->stmts[i]);
    }

    try(ctx->cbs.blk(ctx, expr));

    exit_scope(ctx);

    return true;
}

static bool iter_if(AstIterCtx *ctx, Expr *expr) {
    assert(expr->kind == ExprKind_If);

    iter_expr(ctx, expr->if_expr.condition);
    iter_blk(ctx, expr->if_expr.then);
    if (expr->if_expr.otherwise != null) {
        iter_blk(ctx, expr->if_expr.then);
    }

    return ctx->cbs.if_(ctx, expr);
}

static bool iter_un_op(AstIterCtx *ctx, Expr *expr) {
    assert(expr->kind == ExprKind_UnOp);

    return iter_expr(ctx, expr->unary_op.expr);
}

static bool iter_bin_op(AstIterCtx *ctx, Expr *expr) {
    assert(expr->kind == ExprKind_BinOp);

    try(iter_expr(ctx, expr->bin_op.left));
    try(iter_expr(ctx, expr->bin_op.right));

    return ctx->cbs.bin_op(ctx, expr);
}

static bool iter_let(AstIterCtx *ctx, Expr *expr) {
    assert(expr->kind == ExprKind_Let);

    if (expr->let.type != null) {
        try(ctx->cbs.type(ctx, expr->let.type));
    }

    try(iter_expr(ctx, expr->let.value));

    return ctx->cbs.let(ctx, expr);
}

static bool iter_switch(AstIterCtx *ctx, Expr *expr) {
    assert(expr->kind == ExprKind_Switch);

    try(iter_expr(ctx, expr->switch_expr.condition));
    dynarray_foreach(expr->switch_expr.branches, i) {
        SwitchBranch *branch = &expr->switch_expr.branches[i];
        try(iter_expr(ctx, branch->then));
    }

    return true;
}

static bool iter_ret(AstIterCtx *ctx, Expr *expr) {
    assert(expr->kind == ExprKind_Ret);

    if (expr->ret.expr == null) {
        return true;
    }

    try(iter_expr(ctx, expr->ret.expr));

    return ctx->cbs.ret(ctx, expr);
}

static bool iter_fn_call(AstIterCtx *ctx, Expr *expr) {
    assert(expr->kind == ExprKind_FnCall);

    dynarray_foreach(expr->fn_call.arguments, i) {
        try(iter_expr(ctx, expr->fn_call.arguments[i]));
    }

    return true;
}

static bool iter_loop(AstIterCtx *ctx, Expr *expr) {
    assert(expr->kind == ExprKind_Loop);

    return iter_expr(ctx, expr->loop.body);
}

static bool iter_asm(AstIterCtx *ctx, Expr *expr) {
    assert(expr->kind == ExprKind_Asm);

    dynarray_foreach(expr->asm.inputs, i) {
        try(iter_expr(ctx, expr->asm.inputs[i].val));
    }

    return true;
}

static bool iter_cast(AstIterCtx *ctx, Expr *expr) {
    try(ctx->cbs.type(ctx, expr->cast.to));
    return iter_expr(ctx, expr->cast.expr);
}

bool iter_expr(AstIterCtx *ctx, Expr *expr) {
    switch (expr->kind) {
    case ExprKind_StringLit:   return ctx->cbs.str_lit(ctx, expr);
    case ExprKind_NumberLit:   return ctx->cbs.num_lit(ctx, expr);
    case ExprKind_BoolLit:     return ctx->cbs.bool_lit(ctx, expr);
    case ExprKind_Symbol:      return ctx->cbs.sym(ctx, expr);
    case ExprKind_If:          return iter_if(ctx, expr);
    case ExprKind_Block:       return iter_blk(ctx, expr);
    case ExprKind_UnOp:        return iter_un_op(ctx, expr);
    case ExprKind_BinOp:       return iter_bin_op(ctx, expr);
    case ExprKind_Let:         return iter_let(ctx, expr);
    case ExprKind_Switch:      return iter_switch(ctx, expr);
    case ExprKind_Ret:         return iter_ret(ctx, expr);
    case ExprKind_FnCall:      return iter_fn_call(ctx, expr);
    case ExprKind_Loop:        return iter_loop(ctx, expr);
    case ExprKind_Break:       return ctx->cbs.break_(ctx, expr);
    case ExprKind_Continue:    return ctx->cbs.cont(ctx, expr);
    case ExprKind_Unreachable: return ctx->cbs.unreachable(ctx, expr);
    case ExprKind_Asm:         return iter_asm(ctx, expr);
    case ExprKind_Cast:        return iter_cast(ctx, expr);
    }
}

static bool iter_fn_def(AstIterCtx *ctx, Item *item, bool scanning) {
    assert(item->kind == ItemKind_FnDef);

    FnDef *fn_def = &item->fn_definition;

    if (scanning) {
        return ctx->cbs.scan_fn_def(ctx, item);
    }

    enter_scope(ctx, &fn_def->ns);

    iter_blk(ctx, fn_def->body);

    exit_scope(ctx);

    try(ctx->cbs.fn_def(ctx, item));

    return true;
}

bool iter_item(AstIterCtx *ctx, Item *item, bool scanning) {
    switch (item->kind) {
    case ItemKind_FnDef:
        return iter_fn_def(ctx, item, scanning);

    case ItemKind_ExternFn:
        return scanning
            ? ctx->cbs.scan_extern_fn(ctx, item)
            : ctx->cbs.extern_fn(ctx, item);

    case ItemKind_Use:
        return scanning
            ? ctx->cbs.scan_use(ctx, item)
            : ctx->cbs.use(ctx, item);

    case ItemKind_Const:
        return scanning
            ? ctx->cbs.scan_const(ctx, item)
            : ctx->cbs.const_(ctx, item);
    }
}

bool iter_stmt(AstIterCtx *ctx, Stmt *stmt) {
    switch (stmt->kind) {
    case StmtKind_NakedExpr:
    case StmtKind_Expr:
        return iter_expr(ctx, stmt->expr);

    default:
        chn_warn("unhandled stmt");
        return false;
    }
}

static void iter_module(AstIterCtx *ctx, Module *module, bool scanning) {
    ctx->cur_module = module;

    enter_scope(ctx, &module->ns);

    dynarray_foreach(module->ast->items, i) {
        if (!iter_item(ctx, module->ast->items[i], scanning)) return;
    }

    exit_scope(ctx);
}

static bool iter_default_cb(AstIterCtx* ctx, void* a) {
    (void)ctx;
    (void)a;
    return true;
}

void iter_ast(Compiler *compiler, AstIterCbs cbs, void *data) {
    AstIterCtx ctx;
    ctx.data = data;
    ctx.cbs = cbs;
    ctx.cur_ns = &compiler->ns;
    ctx.compiler = compiler;

    // fill NULL callbacks
    size_t n_cbs = sizeof(AstIterCbs) / sizeof(AstIterVoidCb);
    AstIterVoidCb *ptr = (AstIterVoidCb*)&ctx.cbs;
    for (size_t i = 0; i < n_cbs; i += 1, ptr += 1) {
        if (*ptr == null) {
            *ptr = iter_default_cb;
        }
    }

    // TODO: handle errors

    map_foreach(compiler->modules, module) {
        iter_module(&ctx, module, true);
    }}

    map_foreach(compiler->modules, module) {
        iter_module(&ctx, module, false);
    }}
}

