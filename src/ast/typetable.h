#ifndef TYPETABLE_H
#define TYPETABLE_H

#include <chnlib/dynarray.h>
#include <fir.h>
#include <stdbool.h>


typedef usize type_id;

typedef struct TypeEntry TypeEntry;
typedef enum TypeKind {
    TypeKind_Invalid,

    TypeKind_Void,
    TypeKind_Never,
    TypeKind_Implicit,

    TypeKind_Ptr,

    TypeKind_Fn,

    TypeKind_NumLit,
    TypeKind_Int,
    TypeKind_Float,
    TypeKind_Bool,
} TypeKind;

typedef struct TypePtr {
    type_id to;
    bool is_mut;
} TypePtr;

typedef struct TypeFn {
    DynArray(type_id) param_types;
    type_id ret_type;
} TypeFn;

typedef struct TypeIntegral {
    bool is_signed;
} TypeIntegral;

typedef struct TypeEntry {
    TypeKind kind;
    size_t bits;
    type_id parent_ptr_mut;
    type_id parent_ptr;
    FirType fir_type;

    union {
        TypePtr ptr;
        TypeFn fn;
        TypeIntegral integral;
    };
} TypeEntry;

typedef struct TypeTable {
    DynArray(TypeEntry) types;
} TypeTable;


void typetbl_init(TypeTable *typetbl);
void typetbl_deinit(TypeTable *typetbl);

type_id typetbl_new_type(TypeTable *typetbl, TypeKind kind, size_t bits);
type_id typetbl_new_ptr(TypeTable *typetbl, type_id to, bool is_mut);
type_id typetbl_new_int(TypeTable *typetbl, usize bits, bool is_signed);
type_id typetbl_new_fn(TypeTable *typetbl, type_id ret_type, DynArray(type_id) param_types);

TypeEntry* typetbl_get(TypeTable *typetbl, type_id id);

#endif
