#ifndef AST_H
#define AST_H

#include "ast_type.h"
#include "ast_expr.h"
#include "ast_item.h"

typedef enum StmtKind {
    StmtKind_Expr,
    StmtKind_NakedExpr,
    StmtKind_Item,
} StmtKind;

typedef struct Stmt {
    StmtKind kind;

    union {
        Expr *expr;
        Item *item;
    };
} Stmt;

typedef struct AstRoot {
    DynArray(Item*) items;
} AstRoot;

Expr *ast_expr_new(ExprKind kind);
char *ast_expr_string(Expr *expr);
String ast_expr_modpath_last(ModPath *path);

Item *ast_item_new(ItemKind kind);
char *ast_item_string(Item *item);

Stmt *ast_stmt_new(StmtKind kind);
char *ast_stmt_string(Stmt *stmt);

bool should_remove_stmt_semi(Expr *expression);

#endif
