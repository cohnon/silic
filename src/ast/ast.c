#include "ast.h"

#include <chnlib/logger.h>

char* ast_expr_string(Expr *expr) {
    switch (expr->kind) {
        case ExprKind_StringLit: return "string literal";
        case ExprKind_NumberLit: return "number literal";
        case ExprKind_BoolLit: return "bool literal";
        case ExprKind_Symbol: return "symbol";
        case ExprKind_If: return "if";
        case ExprKind_Block: return "block";
        case ExprKind_UnOp: return "unary operator";
        case ExprKind_BinOp: return "binary operator";
        case ExprKind_Let: return "let";
        case ExprKind_Switch: return "switch";
        case ExprKind_Ret: return "return";
        case ExprKind_FnCall: return "funcation call";
        case ExprKind_Loop: return "loop";
        case ExprKind_Break: return "break";
        case ExprKind_Continue: return "continue";
        case ExprKind_Unreachable: return "unreachable";
        case ExprKind_Asm: return "asm";
        case ExprKind_Cast: return "cast";
    }

    __builtin_unreachable();
}

String ast_expr_modpath_last(ModPath *path) {
    return path->path[dynarray_len(path->path) - 1];
}

char* ast_item_string(Item *item) {
    switch (item->kind) {
        case ItemKind_FnDef: return "function";
        case ItemKind_ExternFn: return "external function";
        case ItemKind_Const: return "const";
        case ItemKind_Use: return "use";
    }

    __builtin_unreachable();
}

char* ast_stmt_string(Stmt *stmt) {
    switch (stmt->kind) {
        case StmtKind_Expr: return "expression";
        case StmtKind_NakedExpr: return "naked expression";
        case StmtKind_Item: return "item";
    }

    __builtin_unreachable();
}

bool should_remove_stmt_semi(Expr *expr) {
    return  expr->kind == ExprKind_If
        or  expr->kind == ExprKind_Switch
        or  expr->kind == ExprKind_Block
        or  expr->kind == ExprKind_Loop
        or  expr->kind == ExprKind_Asm;
}

