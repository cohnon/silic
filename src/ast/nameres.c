#include "nameres.h"

#include "ast/ast.h"
#include "ast/iter.h"

#include <chnlib/maybe.h>
#include <chnlib/logger.h>

static bool nameres_type(AstIterCtx *ctx, Ast_Type *type) {
    switch (type->kind) {
        case AstTypeKind_Void:
        case AstTypeKind_Never: {
            return true;
        }

        case AstTypeKind_Ptr: {
            return nameres_type(ctx, type->ptr.to);
        }

        case AstTypeKind_Symbol: {
            NsEntry *entry = ns_resolve(ctx->cur_ns, &type->symbol.path);
            if (entry == null) {
                chn_error("cannot find type '%.*s'",
                    str_format(ast_expr_modpath_last(&type->symbol.path))
                );
            }

            return true;
        }
    }
}

static bool nameres_sym(AstIterCtx *ctx, Expr *expr) {
    NsEntry *entry = ns_resolve(ctx->cur_ns, &expr->modpath);
    if (entry == null) {
        chn_error("cannot find '%.*s'", str_format(ast_expr_modpath_last(&expr->modpath)));
    }

    return true;
}

static bool nameres_let(AstIterCtx *ctx, Expr *expr) {
    NsEntry *entry = ns_add(ctx->cur_ns, expr->let.name, NsEntryKind_Var);
    if (entry != null) {
        chn_error("redefinition of '%.*s'", str_format(expr->let.name));
    }

    return true;
}

static bool nameres_fn_call(AstIterCtx *ctx, Expr *expr) {
    NsEntry *entry = ns_resolve(ctx->cur_ns, &expr->fn_call.modpath);

    if (entry == null) {
        chn_error("cannot find function '%.*s'",
            str_format(ast_expr_modpath_last(&expr->fn_call.modpath)));
    }

    return true;
}

static bool nameres_fn_def(AstIterCtx *ctx, Item *item) {
    FnDef *fn_def = &item->fn_definition;

    FnSig *fn_sig = &fn_def->signature;
    dynarray_foreach(fn_sig->parameters, i) {
        FnParam *param = &fn_sig->parameters[i];

        ns_add(ctx->cur_ns, param->name, NsEntryKind_Var);
    }

    return true;
}

static bool nameres_scan_fns_and_const(AstIterCtx *ctx, Item *item) {
    NsEntry *existing = ns_add(ctx->cur_ns, item->name, NsEntryKind_Var);
    if (existing != null) {
        chn_error("redefinition of top level item: %.*s", str_format(item->name));
    }

    return true;
}

static bool nameres_scan_use(AstIterCtx *ctx, Item *item) {
    NsEntry *existing = ns_add_module(ctx->cur_ns, item->name, item->use.ns);
    if (existing != null) {
        chn_error("redefinition of top level item: %.*s", str_format(item->name));
    }

    return true;
}

void ir_nameres(Compiler *compiler) {
    AstIterCbs cbs = {
        .type           = nameres_type,

        .scan_fn_def    = nameres_scan_fns_and_const,
        .scan_extern_fn = nameres_scan_fns_and_const,
        .scan_use       = nameres_scan_use,
        .scan_const     = nameres_scan_fns_and_const,

        .fn_def         = nameres_fn_def,

        .sym            = nameres_sym,
        .let            = nameres_let,
        .fn_call        = nameres_fn_call,
    };

    iter_ast(compiler, cbs, null);

}
