#include "nameres.h"

#include "ast/ast.h"

#include <chnlib/maybe.h>
#include <chnlib/logger.h>
#include <assert.h>


typedef struct NameResCtx {
    Compiler  *compiler;
    Module    *cur_module;
    Namespace *cur_ns;
} NameResCtx;


static void enter_scope(NameResCtx *ctx, Namespace *ns) {
    ns_init(ns, ctx->cur_ns);
    ctx->cur_ns = ns;
}

static void exit_scope(NameResCtx *ctx) {
    ctx->cur_ns = ctx->cur_ns->parent;
}


static bool nameres_expr(NameResCtx *ctx, Expr *expr);
static bool nameres_stmt(NameResCtx *ctx, Stmt *stmt);

static bool nameres_type(NameResCtx *ctx, Ast_Type *type) {
    switch (type->kind) {
        case AstTypeKind_Void:
        case AstTypeKind_Never: {
            return true;
        }

        case AstTypeKind_Ptr: {
            return nameres_type(ctx, type->ptr.to);
        }

        case AstTypeKind_Symbol: {
            NsEntry *entry = ns_resolve(ctx->cur_ns, &type->symbol.path);
            if (entry == null) {
                chn_error("cannot find type '%.*s'", str_format(ast_expr_modpath_last(&type->symbol.path)));
            }

            return true;
        }
    }

    __builtin_unreachable();
}

static bool nameres_block(NameResCtx *ctx, Expr *expr) {
    assert(expr->kind == ExprKind_Block);

    enter_scope(ctx, &expr->block.ns);

    dynarray_foreach(expr->block.stmts, i) {
        nameres_stmt(ctx, expr->block.stmts[i]);
    }

    exit_scope(ctx);

    return true;
}

static bool nameres_expr(NameResCtx *ctx, Expr *expr) {
    switch (expr->kind) {
        case ExprKind_StringLit:
        case ExprKind_NumberLit:
        case ExprKind_BoolLit: {
            return true;
        }

        case ExprKind_Symbol: {
            NsEntry *entry = ns_resolve(ctx->cur_ns, &expr->modpath);
            if (entry == null) {
                chn_error("cannot find '%.*s'", str_format(ast_expr_modpath_last(&expr->modpath)));
            }

            return true;
        }

        case ExprKind_If: {
            try(nameres_expr(ctx, expr->if_expr.condition));
            try(nameres_block(ctx, expr->if_expr.then));

            if (expr->if_expr.otherwise != null) {
                try(nameres_block(ctx, expr->if_expr.otherwise));
            }

            return true;
        }

        case ExprKind_Block: {
            return nameres_block(ctx, expr);
        }

        case ExprKind_UnOp: {
            return nameres_expr(ctx, expr->unary_op.expr);
        }

        case ExprKind_BinOp: {
            try(nameres_expr(ctx, expr->bin_op.left));
            return nameres_expr(ctx, expr->bin_op.right);
        }

        case ExprKind_Let: {
            NsEntry *entry = ns_add(ctx->cur_ns, expr->let.name, NsEntryKind_Var);
            if (entry != null) {
                chn_error("redefinition of '%.*s'", str_format(expr->let.name));
            }

            if (expr->let.type != null) {
                try(nameres_type(ctx, expr->let.type));
            }

            return nameres_expr(ctx, expr->let.value);
        }

        case ExprKind_Switch: {
            try(nameres_expr(ctx, expr->switch_expr.condition));
            dynarray_foreach(expr->switch_expr.branches, i) {
                SwitchBranch *branch = &expr->switch_expr.branches[i];
                try(nameres_expr(ctx, branch->then));
            }

            return true;
        }

        case ExprKind_Ret: {
            if (expr->ret.expr == null) {
                return true;
            }

            return nameres_expr(ctx, expr->ret.expr);
        }

        case ExprKind_FnCall: {
            NsEntry *entry = ns_resolve(ctx->cur_ns, &expr->fn_call.modpath);
            if (entry == null) {
                chn_error("cannot find function '%.*s'", str_format(ast_expr_modpath_last(&expr->fn_call.modpath)));
            }

            dynarray_foreach(expr->fn_call.arguments, i) {
                try(nameres_expr(ctx, expr->fn_call.arguments[i]));
            }

            return true;
        }

        case ExprKind_Loop: {
            return nameres_block(ctx, expr->loop.body);
        }

        case ExprKind_Break:
        case ExprKind_Continue:
        case ExprKind_Unreachable: {
            return true;
        }

        case ExprKind_Asm: {
            dynarray_foreach(expr->asm.inputs, i) {
                try(nameres_expr(ctx, expr->asm.inputs[i].val));
            }

            return true;
        }

        case ExprKind_Cast: {
            try(nameres_type(ctx, expr->cast.to));
            return nameres_expr(ctx, expr->cast.expr);
        }

    }

    __builtin_unreachable();
}

static bool nameres_item(NameResCtx *ctx, Item *item) {
    switch (item->kind) {
        case ItemKind_Const:
        case ItemKind_Use:
        case ItemKind_ExternFn: {
            return true;
        }

        case ItemKind_FnDef: {
            FnDef *fn_def = &item->fn_definition;

            enter_scope(ctx, &fn_def->ns);

            FnSig *fn_sig = &fn_def->signature;
            dynarray_foreach(fn_sig->parameters, i) {
                FnParam *param = &fn_sig->parameters[i];

                ns_add(ctx->cur_ns, param->name, NsEntryKind_Var);
            }

            nameres_block(ctx, fn_def->body);

            exit_scope(ctx);

            return true;
        }

        default: {
            chn_error("nameres error: unhandled top level item '%s'", ast_item_string(item));
        }
    }

    __builtin_unreachable();
}

static bool nameres_stmt(NameResCtx *ctx, Stmt *stmt) {
    switch (stmt->kind) {
        case StmtKind_NakedExpr:
        case StmtKind_Expr: {
            return nameres_expr(ctx, stmt->expr);
        }

        case StmtKind_Item: {
            return nameres_item(ctx, stmt->item);
        }
    }

    __builtin_unreachable();
}

static bool nameres_module(NameResCtx *ctx, Module *module) {
    ctx->cur_module = module;

    ctx->cur_ns = &module->ns;

    dynarray_foreach(module->ast->items, i) {
        try(nameres_item(ctx, module->ast->items[i]));
    }

    ctx->cur_ns = null;

    return true;
}

static bool nameres_top_level_items(NameResCtx *ctx, Module *module) {
    enter_scope(ctx, &module->ns);

    dynarray_foreach(module->ast->items, i) {
        Item *item = module->ast->items[i];
        switch (item->kind) {
            case ItemKind_FnDef:
            case ItemKind_ExternFn:
            case ItemKind_Const: {
                NsEntry *existing = ns_add(&module->ns, item->name, NsEntryKind_Var);
                if (existing != null) {
                    chn_error("redefinition of top level item: %.*s", str_format(item->name));
                }
                break;
            }

            case ItemKind_Use: {
                // separate so we can extract modules items
                NsEntry *existing = ns_add_module(&module->ns, item->name, item->use.ns);
                if (existing != null) {
                    chn_error("redefinition of top level item: %.*s", str_format(item->name));
                }
            
                break;
            }

            default: {
                chn_error("nameres error: unhandled top level item '%s'", ast_item_string(item));
            }
        }
    }

    exit_scope(ctx);

    return true;
}

void ir_nameres(Compiler *compiler) {
    NameResCtx ctx;
    ctx.compiler = compiler;
    ctx.cur_ns = &compiler->ns;

    {
        map_foreach(compiler->modules, module) {
            bool success = nameres_top_level_items(&ctx, module);
            if (not success) {
                chn_error("nameres error: module failed");
            }
        }
    }

    {
        map_foreach(compiler->modules, module) {
            bool success = nameres_module(&ctx, module);
            if (not success) {
                chn_error("nameres error: module failed");
            }
        }
    }
}
