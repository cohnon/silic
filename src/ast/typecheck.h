#ifndef AST_TYPECHECK_H
#define AST_TYPECHECK_H

#include "compiler.h"


void ir_typecheck(Compiler *compiler);

#endif
