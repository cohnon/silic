#include "typecheck.h"

#include "ast/ast.h"
#include "ast/namespace.h"
#include "iter.h"

#include <chnlib/map.h>
#include <chnlib/dynarray.h>
#include <chnlib/logger.h>
#include <assert.h>


static bool implicitly_convertable(AstIterCtx *ctx, type_id from, type_id to) {
    if (from == to) { return true; }

    TypeEntry *from_entry = typetbl_get(&ctx->compiler->type_table, from);
    TypeEntry *to_entry = typetbl_get(&ctx->compiler->type_table, to);

    if (from_entry->kind == TypeKind_NumLit
    and (to_entry->kind == TypeKind_Int or to_entry->kind == TypeKind_Float)) {
        return true;
    }

    return false;
}

static type_id typecheck_type(AstIterCtx *ctx, Ast_Type *type) {
    if (type == null) { return TypeKind_Implicit; }

    switch (type->kind) {
        case AstTypeKind_Void: return ctx->compiler->primitives.entry_void;
        case AstTypeKind_Never: return ctx->compiler->primitives.entry_never;

        case AstTypeKind_Ptr: {
            type_id child = typecheck_type(ctx, type->ptr.to);
            TypeEntry *child_entry = typetbl_get(&ctx->compiler->type_table, child);
            type_id existing =
                type->ptr.is_mut
                ? child_entry->parent_ptr_mut
                : child_entry->parent_ptr;

            if (existing == TypeKind_Invalid) { return existing; }

            type_id new = typetbl_new_ptr(&ctx->compiler->type_table, child, type->ptr.is_mut);

            return new;
        }

        case AstTypeKind_Symbol: {
            NsEntry *entry = ns_resolve(ctx->cur_ns, &type->symbol.path);
            assert(entry != null);
            if (entry == null or entry->kind != NsEntryKind_Type) {
                /*errors_new(
                    &compiler->errors,
                    module->path,
                    type->token->position,
                    type->token->span,
                    "use of undeclared type"
                );
                return None;*/
            }

            return entry->type.id;
        }
    }

    __builtin_unreachable();
}

static bool typecheck_str_lit(AstIterCtx *ctx, Expr *expr) {
    expr->codegen.type = ctx->compiler->primitives.entry_c_str;

    return true;
}

static bool typecheck_num_lit(AstIterCtx *ctx, Expr *expr) {
    expr->codegen.type = ctx->compiler->primitives.entry_num_lit;

    return true;
}

static bool typecheck_bool_lit(AstIterCtx *ctx, Expr *expr) {
    expr->codegen.type = ctx->compiler->primitives.entry_bool;

    return true;
}

static bool typecheck_sym(AstIterCtx *ctx, Expr *expr) {
    NsEntry *entry = ns_resolve(ctx->cur_ns, &expr->modpath);
    switch (entry->kind) {
    case NsEntryKind_Var:
        assert(entry->var.type != 0);

        expr->codegen.type = entry->var.type;
        break;

    default: __builtin_unreachable();
    }

    return true;
}

static bool typecheck_if(AstIterCtx *ctx, Expr *expr) {
    assert(expr->kind == ExprKind_If);

    If *if_ = &expr->if_expr;

    type_id cond_type = if_->condition->codegen.type;
    if (cond_type != ctx->compiler->primitives.entry_bool) {
        chn_error("if condition must be a boolean");
    }

    type_id then_type = if_->then->codegen.type;
    if (expr->if_expr.otherwise != null) {
        type_id otherwise_type = if_->otherwise->codegen.type;
        if (then_type != otherwise_type) {
            chn_error("all if branched must resolve to the same type");
        }
    }

    expr->codegen.type = then_type;

    return true;
}

static bool typecheck_blk(AstIterCtx *ctx, Expr *expr) {
    assert(expr->kind == ExprKind_Block);

    Block *blk = &expr->block;

    Stmt *last_stmt = dynarray_last(blk->stmts);
    if (last_stmt->kind == StmtKind_Expr or last_stmt->kind == StmtKind_NakedExpr) {
        expr->codegen.type = dynarray_last(blk->stmts)->expr->codegen.type;
    } else {
        expr->codegen.type = ctx->compiler->primitives.entry_void;
    }

    return true;
}

static bool typecheck_un_op(AstIterCtx *ctx, Expr *expr) {
    assert(expr->kind == ExprKind_UnOp);
    (void)ctx;

    expr->codegen.type = expr->unary_op.expr->codegen.type;

    return true;
}

static bool typecheck_bin_op(AstIterCtx *ctx, Expr *expr) {
    assert(expr->kind == ExprKind_BinOp);
    (void)ctx;

    BinOp *bin_op = &expr->bin_op;
    type_id l_type = bin_op->left->codegen.type;
    type_id r_type = bin_op->right->codegen.type;
    if (l_type != r_type) {
        chn_error("bin op must resolve to same type");
    }

    expr->codegen.type = l_type;

    return true;
}

static bool typecheck_let(AstIterCtx *ctx, Expr *expr) {
    assert(expr->kind == ExprKind_Let);

    NsEntry *entry = ns_local(ctx->cur_ns, expr->let.name);
    assert(entry != null and "invalid ast");

    type_id expl_type = try(typecheck_type(ctx, expr->let.type));
    type_id impl_type = expr->let.value->codegen.type;
    if (expl_type != TypeKind_Implicit
    and not implicitly_convertable(ctx, expl_type, impl_type)) {
        chn_error("let impl_type != expl_type");
    }

    entry->var.type = impl_type;
    assert(entry->var.type != 0);

    expr->codegen.type = ctx->compiler->primitives.entry_void;

    return true;
}

static bool typecheck_ret(AstIterCtx *ctx, Expr *expr) {
    assert(expr->kind == ExprKind_Ret);
    (void)ctx;

    expr->codegen.type = expr->ret.expr->codegen.type;

    return true;
}

static bool typecheck_fn_call(AstIterCtx *ctx, Expr *expr) {
    assert(expr->kind == ExprKind_FnCall);

    NsEntry *ns_entry = ns_resolve(ctx->cur_ns, &expr->fn_call.modpath);
    TypeEntry *type_entry = typetbl_get(&ctx->compiler->type_table, ns_entry->var.type);

    if (type_entry->kind != TypeKind_Fn) {
        chn_error("trying to call non function: %d", type_entry->kind);
    }

    expr->codegen.type = type_entry->fn.ret_type;

    return true;
}

static bool typecheck_void(AstIterCtx *ctx, Expr *expr) {
    expr->codegen.type = ctx->compiler->primitives.entry_void;

    return true;
}

static bool typecheck_never(AstIterCtx *ctx, Expr *expr) {
    expr->codegen.type = ctx->compiler->primitives.entry_never;

    return true;
}

static bool typecheck_cast(AstIterCtx *ctx, Expr *expr) {
    expr->codegen.type = typecheck_type(ctx, expr->cast.to);

    return true;
}

static bool typecheck_fn_def(AstIterCtx *ctx, Item *item) {
    NsEntry *entry = ns_local(ctx->cur_ns, item->name);
    assert(entry != null);

    FnDef *fn_def = &item->fn_definition;
    type_id block_type = fn_def->body->codegen.type;

    TypeEntry *fn_type_entry = typetbl_get(&ctx->compiler->type_table, entry->var.type);
    if (not implicitly_convertable(ctx, block_type, fn_type_entry->fn.ret_type)) {
        chn_error(
            "fn block doesn't match return type: %.*s -> %zu got -> %zu",
            str_format(item->name),
            fn_type_entry->fn.ret_type,
            block_type
        );
    }

    return true;
}

static bool typecheck_const(AstIterCtx *ctx, Item *item) {
    NsEntry *entry = ns_local(ctx->cur_ns, item->name);
    assert(entry != null);

    type_id expl_type = ns_local(ctx->cur_ns, item->name)->var.type;
    type_id impl_type = item->constant.value->codegen.type;

    if (expl_type != impl_type) {
        chn_error("constant type mismatch %zu %zu", expl_type, impl_type);
    }

    return true;
}

static bool typecheck_scan_fn(AstIterCtx *ctx, Item *item) {
    NsEntry *entry = ns_local(ctx->cur_ns, item->name);
    assert(entry != null);
    assert(entry->var.type == TypeKind_Invalid);

    FnSig *fn_sig = item->kind == ItemKind_FnDef
        ? &item->fn_definition.signature
        : &item->extern_fn.signature;

    type_id ret_type = fn_sig->return_type != null
        ? try(typecheck_type(ctx, fn_sig->return_type))
        : TypeKind_Void;

    DynArray(type_id) param_types = dynarray_init();

    dynarray_foreach(fn_sig->parameters, i) {
        FnParam *param = &fn_sig->parameters[i];
        type_id param_type = try(typecheck_type(ctx, param->type));
        dynarray_push(param_types, &param_type);
    }

    type_id fn_type = typetbl_new_fn(&ctx->compiler->type_table, ret_type, param_types);

    entry->var.type = fn_type;

    return true;
}

static bool typecheck_scan_const(AstIterCtx *ctx, Item *item) {
    NsEntry *entry = ns_local(ctx->cur_ns, item->name);
    assert(entry != null);
    assert(entry->var.type == TypeKind_Invalid);


    type_id expl_type = try(typecheck_type(ctx, item->constant.type));
    if (expl_type == TypeKind_Implicit) {
        chn_error("constant type must be explicit: %.*s", str_format(item->name));
    }

    entry->var.type = expl_type;

    return true;
}

void ir_typecheck(Compiler *compiler) {
    AstIterCbs cbs = {
        .scan_fn_def    = typecheck_scan_fn,
        .scan_extern_fn = typecheck_scan_fn,
        .scan_const     = typecheck_scan_const,

        .fn_def         = typecheck_fn_def,
        .const_         = typecheck_const,

        .str_lit        = typecheck_str_lit,
        .num_lit        = typecheck_num_lit,
        .bool_lit       = typecheck_bool_lit,
        .sym            = typecheck_sym,
        .if_            = typecheck_if,
        .blk            = typecheck_blk,
        .un_op          = typecheck_un_op,
        .bin_op         = typecheck_bin_op,
        .let            = typecheck_let,
        .switch_        = typecheck_let,
        .ret            = typecheck_ret,
        .fn_call        = typecheck_fn_call,
        .loop           = typecheck_void,
        .break_         = typecheck_void,
        .cont           = typecheck_void,
        .unreachable    = typecheck_never,
        .asm            = typecheck_void,
        .cast           = typecheck_cast,
    };

    iter_ast(compiler, cbs, null);

}
