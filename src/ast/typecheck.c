#include "typecheck.h"

#include "module.h"
#include "ast/ast.h"
#include "ast/namespace.h"

#include <chnlib/map.h>
#include <chnlib/dynarray.h>
#include <chnlib/logger.h>
#include <assert.h>


typedef struct TypeCheckCtx {
    Compiler *compiler;
    Module *cur_module;
    Namespace *cur_ns;
} TypeCheckCtx;

static void enter_scope(TypeCheckCtx *ctx, Namespace *ns) {
    assert(ns->symbols != null);
    ctx->cur_ns = ns;
}

static void exit_scope(TypeCheckCtx *ctx) {
    ctx->cur_ns = ctx->cur_ns->parent;
}


static type_id typecheck_expr(TypeCheckCtx *ctx, Expr *expr);
static type_id typecheck_stmt(TypeCheckCtx *ctx, Stmt *stmt);

static bool can_convert_implicitly(TypeCheckCtx *ctx, type_id from, type_id to) {
    if (from == to) { return true; }

    TypeEntry *from_entry = typetbl_get(&ctx->compiler->type_table, from);
    TypeEntry *to_entry = typetbl_get(&ctx->compiler->type_table, to);

    if (from_entry->kind == TypeKind_NumLit
    and (to_entry->kind == TypeKind_Int or to_entry->kind == TypeKind_Float)) {
        return true;
    }

    return false;
}

static type_id typecheck_type(TypeCheckCtx *ctx, Ast_Type *type) {
    if (type == null) { return TypeKind_Implicit; }

    switch (type->kind) {
        case AstTypeKind_Void: return ctx->compiler->primitives.entry_void;
        case AstTypeKind_Never: return ctx->compiler->primitives.entry_never;

        case AstTypeKind_Ptr: {
            type_id child = typecheck_type(ctx, type->ptr.to);
            TypeEntry *child_entry = typetbl_get(&ctx->compiler->type_table, child);
            type_id existing =
                type->ptr.is_mut
                ? child_entry->parent_ptr_mut
                : child_entry->parent_ptr;

            if (existing == TypeKind_Invalid) { return existing; }

            type_id new = typetbl_new_ptr(&ctx->compiler->type_table, child, type->ptr.is_mut);

            return new;
        }

        case AstTypeKind_Symbol: {
            NsEntry *entry = ns_resolve(ctx->cur_ns, &type->symbol.path);
            if (entry == null or entry->kind != NsEntryKind_Type) {
                /*errors_new(
                    &compiler->errors,
                    module->path,
                    type->token->position,
                    type->token->span,
                    "use of undeclared type"
                );
                return None;*/
            }

            return entry->type.id;
        }
    }

    __builtin_unreachable();
}

static type_id typecheck_block(TypeCheckCtx *ctx, Expr *expr) {
    assert(expr->kind == ExprKind_Block);

    Block *block = &expr->block;

    enter_scope(ctx, &block->ns);

    for (usize i = 0; i < dynarray_len(block->stmts) - 1; i += 1) {
        try(typecheck_stmt(ctx, expr->block.stmts[i]));
    }

    type_id resolve_type = try(typecheck_stmt(ctx, dynarray_last(block->stmts)));

    exit_scope(ctx);

    return resolve_type;
}

static type_id typecheck_expr(TypeCheckCtx *ctx, Expr *expr) {
    switch (expr->kind) {
        case ExprKind_StringLit: {
            expr->codegen.type = ctx->compiler->primitives.entry_c_str;
            break;
         }

        case ExprKind_NumberLit: {
            expr->codegen.type = ctx->compiler->primitives.entry_num_lit;
            break;
        }

        case ExprKind_BoolLit: {
            expr->codegen.type = ctx->compiler->primitives.entry_bool;
            break;
        }
        
        case ExprKind_Symbol: {
            NsEntry *entry = ns_resolve(ctx->cur_ns, &expr->modpath);
            switch (entry->kind) {
            case NsEntryKind_Var: {
                expr->codegen.type = entry->var.type;
                break;
            }

            default: __builtin_unreachable();
            }

            break;
        }

        case ExprKind_If: {
            type_id cond_type = try(typecheck_expr(ctx, expr->if_expr.condition));
            if (cond_type != ctx->compiler->primitives.entry_bool) {
                chn_error("if condition must be a boolean");
            }

            type_id then_type = try(typecheck_block(ctx, expr->if_expr.then));
            if (expr->if_expr.otherwise != null) {
                type_id otherwise_type = try(typecheck_block(ctx, expr->if_expr.otherwise));
                if (then_type != otherwise_type) {
                    chn_error("all if branched must resolve to the same type");
                }
            }

            expr->codegen.type = then_type;
            break;
        }

        case ExprKind_Block: {
            expr->codegen.type = try(typecheck_block(ctx, expr));
            break;
        }

        case ExprKind_UnOp: {
            expr->codegen.type = try(typecheck_expr(ctx, expr->unary_op.expr));
            break;
        }

        case ExprKind_BinOp: {
            type_id l_type = try(typecheck_expr(ctx, expr->bin_op.left));
            type_id r_type = try(typecheck_expr(ctx, expr->bin_op.right));
            if (l_type != r_type) {
                chn_error("binary operands must resolve to same type");
            }

            expr->codegen.type = l_type;
            break;
        }

        case ExprKind_Let: {
            NsEntry *entry = ns_local(ctx->cur_ns, expr->let.name);
            assert(entry != null and "invalid ast");

            type_id expl_type = try(typecheck_type(ctx, expr->let.type));
            type_id impl_type = try(typecheck_expr(ctx, expr->let.value));
            if (expl_type != TypeKind_Implicit and not can_convert_implicitly(ctx, expl_type, impl_type)) {
                chn_error("let impl_type != expl_type");
            }

            expr->codegen.type = impl_type;
            entry->var.type = impl_type;
            break;
        }

        case ExprKind_Switch: {
            break;
        }

        case ExprKind_Ret: {
            expr->codegen.type = try(typecheck_expr(ctx, expr->ret.expr));
            break;
        }

        case ExprKind_FnCall: {
            NsEntry *ns_entry = ns_resolve(ctx->cur_ns, &expr->fn_call.modpath);
            TypeEntry *type_entry = typetbl_get(&ctx->compiler->type_table, ns_entry->var.type);
            if (type_entry->kind != TypeKind_Fn) {
                chn_error("trying to call non function: %d", type_entry->kind);
            }

            dynarray_foreach(expr->fn_call.arguments, i) {
                Expr *arg = expr->fn_call.arguments[i];
                try(typecheck_expr(ctx, arg));
            }

            expr->codegen.type = type_entry->fn.ret_type;
            break;
        }

        case ExprKind_Loop: {
            expr->codegen.type = ctx->compiler->primitives.entry_void;
            break;
        }

        case ExprKind_Break:
        case ExprKind_Continue: {
            expr->codegen.type = ctx->compiler->primitives.entry_void;
            break;
        }

        case ExprKind_Unreachable: {
            expr->codegen.type = ctx->compiler->primitives.entry_never;
            break;
        }

        case ExprKind_Asm: {
            expr->codegen.type = ctx->compiler->primitives.entry_isize;
            break;
        }

        case ExprKind_Cast: {
            expr->codegen.type = try(typecheck_type(ctx, expr->cast.to));
            break;
        }
    }

    if (expr->codegen.type == TypeKind_Invalid) {
        chn_error("invalid type: '%s'", ast_expr_string(expr));
    }

    return expr->codegen.type;
}

static type_id typecheck_item(TypeCheckCtx *ctx, Item *item, bool scanning) {
    switch (item->kind) {
        case ItemKind_Use: {
            break;
        }

        case ItemKind_ExternFn:
        case ItemKind_FnDef: {
            NsEntry *entry = ns_local(ctx->cur_ns, item->name);
            assert(entry != null);

            // set signature type
            if (entry->var.type == TypeKind_Invalid) {
                FnSig *fn_sig =
                    item->kind == ItemKind_FnDef
                    ? &item->fn_definition.signature
                    : &item->extern_fn.signature;

                type_id ret_type = try(typecheck_type(ctx, fn_sig->return_type));
                DynArray(type_id) param_types = dynarray_init();

                dynarray_foreach(fn_sig->parameters, i) {
                    FnParam *param = &fn_sig->parameters[i];
                    type_id param_type = try(typecheck_type(ctx, param->type));
                    dynarray_push(param_types, &param_type);
                }

                type_id fn_type = typetbl_new_fn(&ctx->compiler->type_table, ret_type, param_types);
                entry->var.type = fn_type;
            }


            if (not scanning and item->kind == ItemKind_FnDef) {
                enter_scope(ctx, &item->fn_definition.ns);

                type_id block_type = try(typecheck_block(ctx, item->fn_definition.body));

                TypeEntry *fn_type_entry = typetbl_get(&ctx->compiler->type_table, entry->var.type);
                if (not can_convert_implicitly(ctx, block_type, fn_type_entry->fn.ret_type)) {
                    chn_error(
                        "fn block doesn't match return type %.*s %zu %zu",
                        str_format(item->name),
                        fn_type_entry->fn.ret_type,
                        block_type
                    );
                }

                exit_scope(ctx);
            }


            break;
        }

        case ItemKind_Const: {
            NsEntry *entry = ns_local(ctx->cur_ns, item->name);

            // set explicit type
            if (entry->var.type == TypeKind_Invalid) {
                type_id expl_type = try(typecheck_type(ctx, item->constant.type));

                if (expl_type == TypeKind_Implicit) {
                    chn_error("constant type must be explicit: %.*s", str_format(item->name));
                }

                entry->var.type = expl_type;
            }

            if (not scanning) {
                type_id expl_type = ns_local(ctx->cur_ns, item->name)->var.type;
                type_id impl_type = try(typecheck_expr(ctx, item->constant.value));

                if (expl_type != impl_type) {
                    chn_error("constant type mismatch %zu %zu", expl_type, impl_type);
                }
            }

            break;
        }
    }

    return ctx->compiler->primitives.entry_void;
}

static type_id typecheck_stmt(TypeCheckCtx *ctx, Stmt *stmt) {
    switch (stmt->kind) {
        case StmtKind_NakedExpr:
        case StmtKind_Expr: {
            return typecheck_expr(ctx, stmt->expr);
        }

        case StmtKind_Item: {
            return typecheck_item(ctx, stmt->item, true);
        }
    }

    __builtin_unreachable();
}

static bool typecheck_module(TypeCheckCtx *ctx, Module *module, bool scanning) {
    ctx->cur_module = module;

    enter_scope(ctx, &module->ns);

    dynarray_foreach(module->ast->items, i) {
        try(typecheck_item(ctx, module->ast->items[i], scanning));
    }

    exit_scope(ctx);

    return true;
}

void ir_typecheck(Compiler *compiler) {
    TypeCheckCtx ctx;
    ctx.compiler = compiler;

    { // top level decls
        map_foreach(compiler->modules, module) {
            if (not typecheck_module(&ctx, module, true)) {
                chn_warn("typecheck error: scanning failed");
            }
        }
    }

    {
        map_foreach(compiler->modules, module) {
            if (not typecheck_module(&ctx, module, false)) {
                chn_warn("typecheck error: definitions failed");
            }
        }
    }
}
