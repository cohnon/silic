#ifndef NAMESPACE_H 
#define NAMESPACE_H 

#include "ast/typetable.h"
#include "mir/mir.h"

#include <chnlib/map.h>
#include <chnlib/str.h>


typedef struct Namespace Namespace;

typedef enum NsEntryKind {
    NsEntryKind_Mod,
    NsEntryKind_Type,
    NsEntryKind_Var,
} NsEntryKind;

typedef struct NsEntryMod {
    Namespace *ns;
} NsEntryMod;

typedef struct NsEntryVar {
    type_id type;
} NsEntryVar;

typedef struct NsEntryType {
    type_id id;
} NsEntryType;

typedef struct NsEntryMirStack {
    MirVarRef block;
    MirVarRef val;
} NsEntryMirStack;

typedef struct NsEntry {
    NsEntryKind kind;
    String mangled_name;
    MirVarRef mir_ref;
    union {
        NsEntryMod mod;
        NsEntryType type;
        NsEntryVar var;
    };
} NsEntry;

typedef struct Namespace {
    struct Namespace *parent;
    Map(NsEntry) symbols;
} Namespace;

void ns_init(Namespace *ns, Namespace *parent);

NsEntry* ns_add(Namespace *ns, String name, NsEntryKind kind);
NsEntry* ns_add_module(Namespace *ns, String name, Namespace *to);
NsEntry* ns_add_type(Namespace *ns, String name, type_id type);

NsEntry* ns_local(Namespace *ns, String name);

typedef struct ModPath ModPath;
NsEntry* ns_resolve(Namespace *ns, ModPath *modpath);

#endif
