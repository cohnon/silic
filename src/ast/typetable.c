#include "typetable.h"

void typetbl_init(TypeTable *typetbl) {
    typetbl->types = dynarray_init();
    // type_id = 0 is invalid
    TypeEntry *entry = dynarray_add(typetbl->types);
    entry->kind = TypeKind_Invalid;
}

void typetbl_deinit(TypeTable *typetbl) {
    dynarray_deinit(typetbl->types);
}

type_id typetbl_new_type(TypeTable *typetbl, TypeKind kind, size_t bits) {
    TypeEntry *entry = dynarray_add(typetbl->types);
    entry->parent_ptr = 0;
    entry->parent_ptr_mut = 0;
    entry->kind = kind;
    entry->bits = bits;

    return dynarray_len(typetbl->types) - 1;
}

type_id typetbl_new_ptr(TypeTable *typetbl, type_id to, bool is_mut) {
    type_id new = typetbl_new_type(typetbl, TypeKind_Ptr, 64);
    TypeEntry *new_entry = &typetbl->types[new];
    new_entry->ptr.to = to;
    new_entry->ptr.is_mut = is_mut;

    TypeEntry *to_entry = &typetbl->types[to];

    type_id existing_ptr = is_mut ? to_entry->parent_ptr_mut : to_entry->parent_ptr;
    if (existing_ptr != 0) {
        return existing_ptr;
    }

    if (is_mut) {
        to_entry->parent_ptr_mut = new;
    } else {
        to_entry->parent_ptr = new;
    }

    return new;
}

type_id typetbl_new_int(TypeTable *typetbl, usize bits, bool is_signed) {
    type_id new = typetbl_new_type(typetbl, TypeKind_Int, bits);

    TypeEntry *new_entry = &typetbl->types[new];
    new_entry->integral.is_signed = is_signed;

    return new;
}

type_id typetbl_new_fn(TypeTable *typetbl, type_id ret_type, DynArray(type_id) param_types) {
    type_id new = typetbl_new_type(typetbl, TypeKind_Fn, 0);

    TypeEntry *new_entry = &typetbl->types[new];
    new_entry->fn.ret_type = ret_type;
    new_entry->fn.param_types = param_types;

    return new;
}

TypeEntry* typetbl_get(TypeTable *typetbl, type_id id) {
    return &typetbl->types[id];
}
