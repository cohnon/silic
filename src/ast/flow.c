#include "flow.h"

#include <chnlib/logger.h>


typedef struct FlowCtx {
    Compiler *compiler;

    Expr *cur_block;
    bool in_loop;
} FlowCtx;

static bool flow_stmt(FlowCtx *ctx, Stmt *stmt);
static bool flow_expr(FlowCtx *ctx, Expr *expr);

static bool flow_block(FlowCtx *ctx, Expr *expr, bool is_loop) {
    bool is_outer_loop = is_loop and not ctx->in_loop; 
    if (is_outer_loop) {
        ctx->in_loop = true;
    }

    dynarray_foreach(expr->block.stmts, i) {
        try(flow_stmt(ctx, expr->block.stmts[i]));
    }

    if (is_outer_loop) {
        ctx->in_loop = false;
    }

    return true;
}

static bool flow_expr(FlowCtx *ctx, Expr *expr) {
    switch (expr->kind) {
        case ExprKind_Loop: {
            try(flow_block(ctx, expr->loop.body, true));
            break;
        }

        case ExprKind_Continue:
        case ExprKind_Break: {
            if (not ctx->in_loop) {
                chn_error("cannot use %s outside of loops", ast_expr_string(expr));
            }
            break;
        }

        default: break; // nothing :P
    }

    return true;
}

static bool flow_item(FlowCtx *ctx, Item *item) {
    switch (item->kind) {
        case ItemKind_Use:
        case ItemKind_ExternFn: {
            return true;
        }

        case ItemKind_Const: {
            try(flow_expr(ctx, item->constant.value));
            break;
        }

        case ItemKind_FnDef: {
            try(flow_block(ctx, item->fn_definition.body, false));
            break;
        }
    }

    return true;
}

static bool flow_stmt(FlowCtx *ctx, Stmt *stmt) {
    (void)ctx;
    (void)stmt;
    return false;
}

static bool flow_module(FlowCtx *ctx, Module *module) {
    dynarray_foreach(module->ast->items, i) {
        try(flow_item(ctx, module->ast->items[i]));
    }

    return true;
}

void ir_flow(Compiler *compiler) {
    FlowCtx ctx = { 0 };
    ctx.compiler = compiler;

    map_foreach(compiler->modules, module) {
        flow_module(&ctx, module);
    }}
}
