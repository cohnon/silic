#ifndef AST_ITER_H
#define AST_ITER_H

#include "ast.h"
#include "compiler.h"


typedef struct AstIterCtx AstIterCtx;

typedef bool(*AstIterVoidCb)(AstIterCtx*, void*);
typedef bool(*AstIterTypeCb)(AstIterCtx*, Ast_Type*);
typedef bool(*AstIterItemCb)(AstIterCtx*, Item*);
typedef bool(*AstIterExprCb)(AstIterCtx*, Expr*);

typedef struct AstIterCbs {
    AstIterTypeCb type;

    AstIterItemCb use;
    AstIterItemCb fn_def;
    AstIterItemCb extern_fn;
    AstIterItemCb const_;

    AstIterItemCb scan_use;
    AstIterItemCb scan_fn_def;
    AstIterItemCb scan_extern_fn;
    AstIterItemCb scan_const;

    AstIterExprCb str_lit;
    AstIterExprCb num_lit;
    AstIterExprCb bool_lit;
    AstIterExprCb sym;
    AstIterExprCb if_;
    AstIterExprCb blk;
    AstIterExprCb un_op;
    AstIterExprCb bin_op;
    AstIterExprCb let;
    AstIterExprCb switch_;
    AstIterExprCb ret;
    AstIterExprCb fn_call;
    AstIterExprCb loop;
    AstIterExprCb break_;
    AstIterExprCb cont;
    AstIterExprCb unreachable;
    AstIterExprCb asm;
    AstIterExprCb cast;
} AstIterCbs;

typedef struct AstIterCtx {
    Module     *cur_module;
    Namespace  *cur_ns;
    Compiler   *compiler;
    void       *data;
    AstIterCbs cbs;
} AstIterCtx;

void iter_ast(Compiler *compiler, AstIterCbs cbs, void *data);

bool iter_item(AstIterCtx*, Item*, bool scanning);
bool iter_stmt(AstIterCtx*, Stmt*);
bool iter_expr(AstIterCtx*, Expr*);
bool iter_blk(AstIterCtx*, Expr*);

#endif
