#include "module.h"

void module_init(Module* module, String path, String src) {
    module->path = path;
    module->src = src;

    module->name.len = 0;

    module->token_list = dynarray_init();
    module->imports = dynarray_init();
    module->errors = dynarray_init();
}

void module_deinit(Module* module) {
    dynarray_deinit(module->token_list);
    dynarray_deinit(module->imports);
    dynarray_deinit(module->errors);
}
