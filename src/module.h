#ifndef MODULE_H
#define MODULE_H

#include "error_msg.h"
#include "ast/ast.h"
#include "ast/namespace.h"

#include <chnlib/dynarray.h>
#include <chnlib/str.h>


typedef struct Module Module;

typedef struct ModImport {
    Use *use;
    Module *module;
} ModImport;

typedef struct ModExport {
    Item *item;
} ModExport;

typedef struct Module {
    String name;
    String path;
    String src;

    DynArray(Token) token_list;
    AstRoot *ast;

    DynArray(ModImport) imports;
    DynArray(ModExport) exports;

    DynArray(ErrorMsg) errors;

    Namespace ns;
} Module;

void module_init(Module *module, String path, String src);
void module_deinit(Module *module);

#endif
