#include "lower.h"

#include "mir/mir.h"

#include <chnlib/dynarray.h>
#include <chnlib/logger.h>
#include <chnlib/map.h>
#include <assert.h>


typedef struct LowerCtx {
    Compiler *compiler;
    Module *cur_module;
    Namespace *cur_ns;
    MirCtx *mir;

    // used for break and continue
    MirVarRef next_block;
    MirVarRef prev_block;
} LowerCtx;


static void enter_scope(LowerCtx *ctx, Namespace *ns) {
    ctx->cur_ns = ns;
}

static void exit_scope(LowerCtx *ctx) {
    assert(ctx->cur_ns->parent != null and "HI");
    ctx->cur_ns = ctx->cur_ns->parent;
}


static MirVarRef lower_expr(LowerCtx *ctx, Expr *expr);
static void lower_stmt(LowerCtx *ctx, Stmt *stmt);

static MirTypeRef convert_type(LowerCtx *ctx, type_id id) {
    TypeEntry *entry = typetbl_get(&ctx->compiler->type_table, id);
    switch (entry->kind) {
    case TypeKind_Invalid:
    case TypeKind_Void: return ctx->mir->primitives.invalid_ref;
    case TypeKind_Ptr: return ctx->mir->primitives.ptr_ref;
    case TypeKind_NumLit:
    case TypeKind_Int: return ctx->mir->primitives.i32_ref;
    default: chn_warn("unhandled type %d", entry->kind); return ctx->mir->primitives.invalid_ref;
    }
}

static String mangle_name(LowerCtx *ctx, Item *item) {
    if (item->kind == ItemKind_ExternFn) {
        return item->name;
    }

    String name = item->name;

    String mod_path = ctx->cur_module->path;

    // 'path/to/module' + '_' + 'item_name'
    // 'path_to_module_item_name'
    String mangled_name = str_init(mod_path.len + name.len + 1);

    for (usize i = 0; i < mod_path.len; i += 1) {
        mangled_name.ptr[i] =
            mod_path.ptr[i] == '/'
            ? '_'
            : mod_path.ptr[i];
    }

    mangled_name.ptr[mod_path.len] = '_';

    for (usize i = 0; i < name.len; i += 1) {
        mangled_name.ptr[i + mod_path.len + 1] = name.ptr[i];
    }

    return mangled_name;
}

static MirVarRef lower_block(LowerCtx *ctx, Expr *expr, bool implicit_return) {
    assert(expr->kind == ExprKind_Block);

    Block *block = &expr->block;

    if (dynarray_len(block->stmts) == 0) {
        return mir_invalid();
    }

    enter_scope(ctx, &block->ns);

    for (usize i = 0; i < dynarray_len(block->stmts) - 1; i += 1) {
        lower_stmt(ctx, block->stmts[i]);
    }


    MirVarRef ref = mir_invalid();

    Stmt *last_stmt = dynarray_last(block->stmts);
    if (last_stmt->kind == StmtKind_NakedExpr) {
        ref = lower_expr(ctx, last_stmt->expr);
    } else {
        lower_stmt(ctx, last_stmt);
    }

    if (implicit_return) {
        bool last_stmt_is_expr = 
            last_stmt->kind == StmtKind_Expr or last_stmt->kind == StmtKind_NakedExpr;

        chn_debug("%d %d %d", last_stmt_is_expr, last_stmt->expr->kind != ExprKind_Ret, last_stmt->expr->codegen.type == TypeKind_Void);

        if (last_stmt_is_expr and last_stmt->expr->kind != ExprKind_Ret) {
            if (last_stmt->expr->codegen.type == TypeKind_Void) {
                mir_instr_ret_void(ctx->mir);
            } else if (last_stmt->expr->codegen.type == TypeKind_Never) {
                mir_instr_unreachable(ctx->mir);
            } else {
                mir_instr_ret(ctx->mir, ref);
            }
        }
    }

    exit_scope(ctx);

    return ref;
}

static MirVarRef lower_expr(LowerCtx *ctx, Expr *expr) {
    switch (expr->kind) {
        case ExprKind_NumberLit: {
            TypeEntry *entry =
                typetbl_get (
                    &ctx->compiler->type_table,
                    ctx->compiler->primitives.entry_i32
                );

            return mir_const(ctx->mir, entry->mir_ref, expr->number_literal.int_);
        }

        case ExprKind_StringLit: {
            String span = expr->string_literal.span;
            String string_content = str_slice(span.ptr + 1, span.len - 2);
            return mir_string_lit(ctx->mir, string_content, MirLinkage_Priv);
        }

        case ExprKind_BoolLit: {
            TypeEntry *entry =
                typetbl_get (
                    &ctx->compiler->type_table,
                    ctx->compiler->primitives.entry_bool
                );

            return mir_const(ctx->mir, entry->mir_ref, 1);
        }

        case ExprKind_Block: {
            return lower_block(ctx, expr, false);
        }

        case ExprKind_Let: {
            NsEntry *entry = ns_local(ctx->cur_ns, expr->let.name);
            assert(entry != null and "invalid ast");

            MirVarRef val = lower_expr(ctx, expr->let.value);

            if (val.kind != MirVarRefKind_VReg) {
                MirVarRef ref = mir_instr_copy(ctx->mir, val);
                entry->mir_ref = ref;
            } else {
                entry->mir_ref = val;
            }

            return val;
        }

        case ExprKind_BinOp: {
            BinOp *bin_op = &expr->bin_op;

            MirVarRef lhs = lower_expr(ctx, expr->bin_op.left);
            MirVarRef rhs = lower_expr(ctx, expr->bin_op.right);
            MirTypeRef type = convert_type(ctx, expr->codegen.type);

            switch (bin_op->kind) {
            case BinOpKind_Add: return mir_instr_add(ctx->mir, type, lhs, rhs);
            case BinOpKind_Assign: return mir_instr_set(ctx->mir, lhs, rhs);
            default: chn_warn("unhandled binary operator");
            }

            return mir_invalid();
        }

        case ExprKind_Break: {
            mir_instr_jmp(ctx->mir, ctx->next_block);

            return mir_invalid();
        }

        case ExprKind_Continue: {
            mir_instr_jmp(ctx->mir, ctx->prev_block);

            return mir_invalid();
        }

        case ExprKind_Ret: {
            
            if (expr->ret.expr == null) {
                mir_instr_ret_void(ctx->mir);
            } else {
                MirVarRef val = lower_expr(ctx, expr->ret.expr);
                mir_instr_ret(ctx->mir, val);
            }

            return mir_invalid();
        }

        case ExprKind_Loop: {
            //  +------------+
            //  | prev block |
            //  +-----+------+
            //        |
            //  +-----+------+ <--.
            //  | loop block |    |
            //  +-----+------+ -->'
            //        |
            //  +-----+------+
            //  | next block |
            //  +------------+

            MirVarRef loop_block = mir_create_block(ctx->mir);
            MirVarRef next_block = mir_create_block(ctx->mir);

            // update last/prev blocks
            MirVarRef last_prev_block = ctx->prev_block;
            MirVarRef last_next_block = ctx->next_block;
            ctx->prev_block = mir_cur_block(ctx->mir);
            ctx->next_block = next_block;


            mir_instr_jmp(ctx->mir, loop_block);

            mir_insert_block(ctx->mir, loop_block);
            mir_use_block(ctx->mir, loop_block);

            lower_block(ctx, expr->loop.body, false);

            mir_insert_block(ctx->mir, next_block);
            mir_instr_jmp(ctx->mir, loop_block);

            mir_use_block(ctx->mir, next_block);

            // restore last/prev blocks
            ctx->prev_block = last_prev_block;
            ctx->next_block = last_next_block;

            return mir_invalid();
        }

        case ExprKind_If: {
            // +------------+
            // | prev block | ------>.
            // +-----+------+        |
            //       |               |
            // +-----+------+        |
            // | then block | -->.   |
            // +-----+------+    |   |
            //                   |   |
            // +-----+------+    |   |
            // | else block | <--|---'
            // +-----+------+    |
            //       |           |
            // +-----+------+    |
            // | next block | <--'
            // +------------+
            
            MirVarRef then_block = mir_create_block(ctx->mir);
            MirVarRef else_block = mir_create_block(ctx->mir);
            MirVarRef next_block = mir_create_block(ctx->mir);

            MirVarRef cond = lower_expr(ctx, expr->if_expr.condition);
            mir_instr_jeq(ctx->mir, cond, then_block, else_block);

            mir_insert_block(ctx->mir, then_block);
            mir_use_block(ctx->mir, then_block);

            lower_block(ctx, expr->if_expr.then, false);
            mir_instr_jmp(ctx->mir, next_block);

            if (expr->if_expr.otherwise) {
                mir_insert_block(ctx->mir, else_block);
                mir_use_block(ctx->mir, else_block);
                lower_block(ctx, expr->if_expr.otherwise, false);
                mir_instr_jmp(ctx->mir, next_block);
            }

            mir_insert_block(ctx->mir, next_block);
            mir_use_block(ctx->mir, next_block);
            
            return mir_invalid();
        }

        case ExprKind_FnCall: {
            usize args_len = dynarray_len(expr->fn_call.arguments);
            DynArray(MirFnArg) args = dynarray_init_with(MirFnArg, args_len);

            dynarray_foreach(expr->fn_call.arguments, i) {
                Expr *ast_arg = expr->fn_call.arguments[i];
                MirFnArg *arg = dynarray_add(args);
                arg->type = convert_type(ctx, ast_arg->codegen.type);
                arg->val = lower_expr(ctx, ast_arg);
            }

            NsEntry *fn_entry = ns_resolve(ctx->cur_ns, &expr->fn_call.modpath);
            assert(fn_entry != null and fn_entry->kind == NsEntryKind_Var and "invalid ast");

            MirVarRef ret_val = mir_instr_fn_call(ctx->mir, fn_entry->mangled_name, args);

            return ret_val;
        }

        case ExprKind_Symbol: {
            NsEntry *entry = ns_resolve(ctx->cur_ns, &expr->symbol.path);
            assert(entry != null and "invalid ast");

            return entry->mir_ref;
        }

        default: {
            chn_debug("mir lower error: unhandled expression: '%s'", ast_expr_string(expr));
            return mir_invalid();
        }
    }
}

static void lower_item(LowerCtx *ctx, Item *item) {
    switch (item->kind) {
        case ItemKind_Use:
        case ItemKind_Const:
        case ItemKind_ExternFn: {
            break;
        }

        case ItemKind_FnDef: {
            NsEntry *entry = ns_local(ctx->cur_ns, item->name);
            assert(entry != null and "invalid ast");

            entry->mir_ref = mir_fn(ctx->mir, entry->mangled_name, MirLinkage_Priv);

            enter_scope(ctx, &item->fn_definition.ns);

            lower_block(ctx, item->fn_definition.body, true);

            exit_scope(ctx);

            break;
        }
    }
}

static void lower_stmt(LowerCtx *ctx, Stmt *stmt) {
    switch (stmt->kind) {
        case StmtKind_Item: {
            break;
        }
        
        case StmtKind_NakedExpr:
        case StmtKind_Expr: {
            lower_expr(ctx, stmt->expr);
            break;
        }
    }
}

void mir_lower(Compiler *compiler) {
    LowerCtx ctx;
    ctx.compiler = compiler;
    ctx.mir = &compiler->mir;

    {
        map_foreach(compiler->modules, module) {
            ctx.cur_module = module;
            enter_scope(&ctx, &module->ns);

            dynarray_foreach(module->ast->items, i) {
                Item *item = module->ast->items[i];
                NsEntry *entry = ns_local(ctx.cur_ns, item->name);
                assert(entry != null and "invalid ast");

                entry->mangled_name = mangle_name(&ctx, item);
            }
        }
    }

    {
        map_foreach(compiler->modules, module) {
            ctx.cur_module = module;
            enter_scope(&ctx, &module->ns);

            dynarray_foreach(module->ast->items, i) {
                lower_item(&ctx, module->ast->items[i]);
            }
        }
    }
}
