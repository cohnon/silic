#ifndef MIR_SSA_PASS_H
#define MIR_SSA_PASS_H

#include "mir.h"


void mir_ssa_pass(MirCtx *ctx);

#endif
