#include "mir.h"

#include <stdio.h>
#include <chnlib/logger.h>
#include <assert.h>


void mir_init(MirCtx *ctx) {
    ctx->globals = dynarray_init();
    ctx->consts = dynarray_init();
    ctx->fns = dynarray_init();
    ctx->blocks = dynarray_init();
    ctx->instrs = dynarray_init();
    ctx->types = dynarray_init();

    ctx->primitives.invalid_ref = mir_type_invalid(ctx);
    ctx->primitives.i32_ref = mir_type_int(ctx, 32, true);
    ctx->primitives.ptr_ref = mir_type_ptr(ctx);
}

MirVarRef mir_invalid(void) {
    return (MirVarRef){ MirVarRefKind_Invalid, 0 };
}

bool mir_var_eq(MirVarRef a, MirVarRef b) {
    return (a.kind == b.kind) and (a.idx == b.idx);
}

MirVarRef mir_fn(MirCtx *ctx, String name, MirLinkage linkage) {
   
    // create function
    MirFn *new = dynarray_add(ctx->fns);
    new->name = name;
    new->linkage = linkage;
    new->vars = dynarray_init();
    new->blocks = dynarray_init();
    new->instr_id = 0;

    // make reference to function
    MirVarRef ref;
    ref.kind = MirVarRefKind_Glob;
    ref.idx = dynarray_len(ctx->fns) - 1;

    ctx->cur_fn = ref;

    // initialise it with a block
    MirVarRef entry_block = mir_create_block(ctx);
    mir_insert_block(ctx, entry_block);
    mir_use_block(ctx, entry_block);

    return ref;
}

MirVarRef mir_create_block(MirCtx *ctx) {

    // create block
    MirBlock *new = dynarray_add(ctx->blocks);

    new->terminated = false;
    new->seen = false;
    new->phis = dynarray_init();
    new->instrs = dynarray_init();
    new->succs = dynarray_init();
    new->preds = dynarray_init();

    MirVarRef ref;
    ref.kind = MirVarRefKind_Block;
    ref.idx = dynarray_len(ctx->blocks) - 1;

    return ref;
}

void mir_insert_block(MirCtx *ctx, MirVarRef block_ref) {
    MirFn *fn = &ctx->fns[ctx->cur_fn.idx];
    
    MirBlock *block = &ctx->blocks[block_ref.idx];
    block->fn = ctx->cur_fn;
    block->id = dynarray_len(fn->blocks);

    // if entry block
    if (dynarray_len(fn->blocks) == 0) {
        fn->entry = block_ref;
    }

    dynarray_push(fn->blocks, &block_ref);
}

void mir_use_block(MirCtx *ctx, MirVarRef block) {
    assert(block.kind == MirVarRefKind_Block);

    ctx->cur_block = block;
}

MirVarRef mir_cur_block(MirCtx *ctx) {
    return ctx->cur_block;
}

// types
MirTypeRef mir_type_invalid(MirCtx *ctx) {
    MirType *new = dynarray_add(ctx->types);
    new->kind = MirTypeKind_Invalid;
    new->bits = 0;
    new->signed_ = false;

    return (MirTypeRef){
        dynarray_len(ctx->types) - 1
    };
}

MirTypeRef mir_type_ptr(MirCtx *ctx) {
    MirType *new = dynarray_add(ctx->types);
    new->kind = MirTypeKind_Ptr;
    new->bits = 64;
    new->signed_ = false;

    return (MirTypeRef){
        dynarray_len(ctx->types) - 1
    };
}

MirTypeRef mir_type_int(MirCtx *ctx, u32 bits, bool is_signed) {
    MirType *new = dynarray_add(ctx->types);
    new->kind = MirTypeKind_Int;
    new->bits = bits;
    new->signed_ = is_signed;

    return (MirTypeRef){
        dynarray_len(ctx->types) - 1
    };
}

MirTypeRef mir_type_float(MirCtx *ctx, u32 bits) {
    MirType *new = dynarray_add(ctx->types);
    new->kind = MirTypeKind_Float;
    new->bits = bits;

    return (MirTypeRef){
        dynarray_len(ctx->types) - 1
    };
}

static MirBlock* get_cur_block(MirCtx *ctx) {
    MirBlock *cur_block = &ctx->blocks[ctx->cur_block.idx];

    return cur_block;
}

MirVarRef mir_glob(MirCtx *ctx, String name, MirTypeRef type, usize size, void *data, MirLinkage linkage, bool is_string_lit) {
    
    // create global
    MirGlob *glob = dynarray_add(ctx->globals);
    glob->name = name;
    glob->linkage = linkage;
    glob->size = size;
    glob->data = data;
    glob->is_string_lit = is_string_lit;

    // make reference to global
    MirVarRef ref;
    ref.kind = MirVarRefKind_Glob;
    ref.idx = dynarray_len(ctx->globals) - 1;

    return ref;
}

MirVarRef mir_string_lit(MirCtx *ctx, String val, MirLinkage linkage) {
    usize id = dynarray_len(ctx->globals);
    String name = str_from_format("__glob%zu", id);

    return mir_glob(ctx, name, ctx->primitives.ptr_ref, val.len, val.ptr, linkage, true);
}

static MirVarRef create_instr(MirCtx *ctx, MirKind kind) {

    // create instruction
    MirInstr *instr = dynarray_add(ctx->instrs);
    instr->kind = kind;

    // make reference to instruction
    MirVarRef ref;
    ref.kind = MirVarRefKind_VReg;
    ref.idx = dynarray_len(ctx->instrs) - 1;

    return ref;
}

#define ADD_INSTR(ctx, kind)                    \
    MirVarRef ref = create_instr(ctx, kind);       \
    MirInstr *instr = &ctx->instrs[ref.idx];    \
    {                                           \
        MirFn *fn = &ctx->fns[ctx->cur_fn.idx]; \
        instr->id = fn->instr_id++;             \
        MirBlock *block = get_cur_block(ctx);   \
        dynarray_push(block->instrs, &ref);     \
    }

#define ADD_TERMI(ctx, kind)                    \
    if (get_cur_block(ctx)->terminated) return; \
    get_cur_block(ctx)->terminated = true;      \
    MirVarRef ref = create_instr(ctx, kind);    \
    MirInstr *instr = &ctx->instrs[ref.idx];    \
    {                                           \
        MirFn *fn = &ctx->fns[ctx->cur_fn.idx]; \
        instr->id = fn->instr_id++;             \
        get_cur_block(ctx)->termi = ref;        \
    }

static void link_blocks(MirCtx *ctx, MirVarRef from, MirVarRef to) {
    assert(from.kind == MirVarRefKind_Block);
    assert(to.kind == MirVarRefKind_Block);

    MirBlock *from_block = &ctx->blocks[from.idx];
    MirBlock *to_block = &ctx->blocks[to.idx];

    dynarray_push(from_block->succs, &to);
    dynarray_push(to_block->preds, &from);
}

MirVarRef mir_const(MirCtx *ctx, MirTypeRef type, u64 val) {

    // create const
    MirConst *const_ = dynarray_add(ctx->consts);
    const_->type = type;
    const_->int_ = val;

    // make reference to const
    MirVarRef ref;
    ref.kind = MirVarRefKind_Const;
    ref.idx = dynarray_len(ctx->consts) - 1;

    return ref;
}

MirVarRef mir_instr_add(MirCtx *ctx, MirTypeRef type, MirVarRef lhs, MirVarRef rhs) {
    ADD_INSTR(ctx, MirKind_Add);

    instr->type = type;
    instr->args[0] = lhs;
    instr->args[1] = rhs;

    return ref;
}

void mir_instr_jmp(MirCtx *ctx, MirVarRef block) {
    ADD_TERMI(ctx, MirKind_Jmp);

    instr->args[0] = block;

    link_blocks(ctx, ctx->cur_block, block);
}

void mir_instr_jeq(MirCtx *ctx, MirVarRef cond, MirVarRef then_block, MirVarRef else_block) {
    ADD_TERMI(ctx, MirKind_Jeq);

    instr->args[0] = cond;
    instr->args[1] = then_block;
    instr->args[2] = else_block;
    
    link_blocks(ctx, ctx->cur_block, then_block);
    link_blocks(ctx, ctx->cur_block, else_block);
}

void mir_instr_jne(MirCtx *ctx, MirVarRef cond, MirVarRef then_block, MirVarRef else_block) {
    ADD_TERMI(ctx, MirKind_Jne);

    instr->args[0] = cond;
    instr->args[1] = then_block;
    instr->args[2] = else_block;

    link_blocks(ctx, ctx->cur_block, then_block);
    link_blocks(ctx, ctx->cur_block, else_block);
}

void mir_instr_ret(MirCtx *ctx, MirVarRef val) {
    ADD_TERMI(ctx, MirKind_Ret);

    instr->args[0] = val;
}

void mir_instr_ret_void(MirCtx *ctx) {
    ADD_TERMI(ctx, MirKind_Ret);

    instr->args[0] = (MirVarRef){ MirVarRefKind_Void, 0 };
}

void mir_instr_unreachable(MirCtx *ctx) {
    ADD_TERMI(ctx, MirKind_Unreachable);
}


MirVarRef mir_instr_set(MirCtx *ctx, MirVarRef lval, MirVarRef rval) {
    assert(lval.kind == MirVarRefKind_VReg);

    ADD_INSTR(ctx, MirKind_Set);

    instr->args[0] = lval;
    instr->args[1] = rval;

    // add to function variabels if not already
    MirFn *fn = &ctx->fns[ctx->cur_fn.idx];
    bool already_in_vars = false;
    dynarray_foreach(fn->vars, i) {
        MirVarRef var_ref = fn->vars[i];

        if (mir_var_eq(var_ref, lval)) {
            already_in_vars = true;
        }
    }

    if (not already_in_vars) {
        dynarray_push(fn->vars, &lval);
    }

    return ref;
}

MirVarRef mir_instr_copy(MirCtx *ctx, MirVarRef var) {
    ADD_INSTR(ctx, MirKind_Copy);

    instr->args[0] = var;

    return ref;
}

MirVarRef mir_instr_fn_call(MirCtx *ctx, String name, DynArray(MirFnArg) args) {
    ADD_INSTR(ctx, MirKind_Call);

    chn_debug("fn_call args: %zu", dynarray_len(args));

    instr->fn_call.name = name;
    instr->fn_call.args = args;

    return ref;
}

MirVarRef mir_insert_phi(MirCtx *ctx, MirVarRef block_ref) {
    MirVarRef ref = create_instr(ctx, MirKind_Phi);
    MirBlock *block = &ctx->blocks[block_ref.idx];
    MirFn *fn = &ctx->fns[block->fn.idx];

    MirInstr *instr = &ctx->instrs[ref.idx];
    instr->id = fn->instr_id++;
    instr->phi = dynarray_init();

    dynarray_push(block->phis, &ref);

    return ref;
}

MirVarRef mir_insert_assosiated_phi(MirCtx *ctx, MirVarRef block_ref, MirVarRef var) {
    // same as above but puts var in arg0
    MirVarRef ref = create_instr(ctx, MirKind_Phi);
    MirBlock *block = &ctx->blocks[block_ref.idx];
    MirFn *fn = &ctx->fns[block->fn.idx];

    MirInstr *instr = &ctx->instrs[ref.idx];
    instr->id = fn->instr_id++;
    instr->args[0] = var;
    instr->phi = dynarray_init();

    dynarray_push(block->phis, &ref);

    return ref;
}

void mir_phi_add_incoming(MirCtx *ctx, MirVarRef phi, MirVarRef var, MirVarRef from) {
    assert(phi.kind == MirVarRefKind_VReg);

    MirInstr *instr = &ctx->instrs[phi.idx];
    MirPhiArg arg;
    arg.var = var;
    arg.block = from;

    dynarray_push(instr->phi, &arg);
}


// debug

static void mir_debug_type(MirCtx *ctx, MirTypeRef ref) {
    MirType *type = &ctx->types[ref.idx];
    switch (type->kind) {
    case MirTypeKind_Invalid: printf("<invalid>"); break;
    case MirTypeKind_Ptr: printf("ptr"); break;
    case MirTypeKind_Int: printf("i%d", type->bits); break;
    default: chn_warn("unhandled debug type");
    }
}

static void mir_debug_ref(MirCtx *ctx, MirVarRef ref) {
    switch (ref.kind) {
        case MirVarRefKind_VReg: printf("%%%d", ctx->instrs[ref.idx].id); break;
        case MirVarRefKind_Block: printf(".b%d", ctx->blocks[ref.idx].id); break;

        case MirVarRefKind_Const: {
            MirConst *const_ = &ctx->consts[ref.idx];
            printf("%lu", const_->int_);
            break;
        }

        case MirVarRefKind_Glob: {
            MirGlob *glob = &ctx->globals[ref.idx];
            printf("@%.*s", str_format(glob->name));
            break;
        }

        default: printf("<invalid>");
    }
}

static void mir_debug_instr(MirCtx *ctx, MirVarRef ref) {
    assert(ref.kind == MirVarRefKind_VReg and "invalid mir reference");
    MirInstr *instr = &ctx->instrs[ref.idx];

    if (instr->kind == MirKind_Ret) {
        printf("    ret");
        if (instr->args[0].kind != MirVarRefKind_Void) {
            printf(" ");
            mir_debug_ref(ctx, instr->args[0]);
        }

        printf("\n");

        return;

    } else if (instr->kind == MirKind_Unreachable) {
        printf("    unreachable\n");

        return;

    } else if (instr->kind == MirKind_Set) {
        printf("    set ");
        mir_debug_ref(ctx, instr->args[0]);
        printf(" = ");
        mir_debug_ref(ctx, instr->args[1]);
        printf("\n");

        return;
    }

    printf("    %%%d = ", instr->id);

    switch(instr->kind) {
        case MirKind_Add: {
            printf("add ");
            mir_debug_type(ctx, instr->type);
            printf(" ");
            mir_debug_ref(ctx, instr->args[0]);
            printf(" ");
            mir_debug_ref(ctx, instr->args[1]);

            break;
        }

        case MirKind_Copy: {
            printf("copy ");
            mir_debug_ref(ctx, instr->args[0]);

            break;
        }

        case MirKind_Call: {
            printf("call @%.*s(", str_format(instr->fn_call.name));
            usize args_len = dynarray_len(instr->fn_call.args);
            for (usize i = 0; i < args_len; i += 1) {
                mir_debug_type(ctx, instr->fn_call.args[i].type);
                printf(" ");
                mir_debug_ref(ctx, instr->fn_call.args[i].val);

                if (i < args_len - 1) {
                    printf(", ");
                }
            }

            printf(")");

            break;
        }

        case MirKind_Phi: {
            printf("phi ");
            dynarray_foreach(instr->phi, i) {
                MirPhiArg *arg = &instr->phi[i];
                mir_debug_ref(ctx, arg->var);
                printf(" ");
                mir_debug_ref(ctx, arg->block);

                if (i < dynarray_len(instr->phi) - 1) {
                    printf(", ");
                } else {
                    printf(" ");
                }
            }

            break;
        }

        case MirKind_Jmp: {
            assert(instr->args[0].kind == MirVarRefKind_Block and "invalid mir reference");
            printf("jmp ");
            mir_debug_ref(ctx, instr->args[0]);
            break;
        }

        case MirKind_Jeq: {
            assert(instr->args[1].kind == MirVarRefKind_Block and "invalid reference");
            printf("jeq ");
            mir_debug_ref(ctx, instr->args[0]);
            printf(" ");
            mir_debug_ref(ctx, instr->args[1]);
            printf(" ");
            mir_debug_ref(ctx, instr->args[2]);

            break;
        }

        case MirKind_Jne: {
            assert(instr->args[1].kind == MirVarRefKind_Block and "invalid reference");
            printf("jne ");
            mir_debug_ref(ctx, instr->args[0]);
            printf(" ");
            mir_debug_ref(ctx, instr->args[1]);
            printf(" ");
            mir_debug_ref(ctx, instr->args[2]);

            break;
        }

        case MirKind_Set:
        case MirKind_Unreachable:
        case MirKind_Ret: __builtin_unreachable();

        default: printf("unknown"); break;
    }

    printf("\n");
}

static void mir_debug_block(MirCtx *ctx, MirVarRef ref) {
    assert(ref.kind == MirVarRefKind_Block and "invalid mir reference");
    MirBlock *block = &ctx->blocks[ref.idx];

    printf("\n  .b%d:", block->id);

    printf(" preds(");
    dynarray_foreach(block->preds, i) {
        printf(" .b%d ", ctx->blocks[block->preds[i].idx].id);
    }
    printf(" )");

    printf(" succs(");
    dynarray_foreach(block->succs, i) {
        printf(" .b%d", ctx->blocks[block->succs[i].idx].id);
    }
    printf(" )");

    printf("\n");

    if (dynarray_len(block->phis) > 0) {
        dynarray_foreach(block->phis, i) {
            mir_debug_instr(ctx, block->phis[i]);
        }
    }

    dynarray_foreach(block->instrs, i) {
        mir_debug_instr(ctx, block->instrs[i]); }

    mir_debug_instr(ctx, block->termi);
}

static void mir_debug_fn(MirCtx *ctx, MirFn *fn) {
    printf("@%.*s:\n", str_format(fn->name));

    dynarray_foreach(fn->blocks, i) {
        mir_debug_block(ctx, fn->blocks[i]);
    }

    printf("\n");
}

static void mir_debug_glob(MirCtx *ctx, MirGlob *glob) {
    printf("@%.*s ", str_format(glob->name));
    printf("%.*s\n", glob->size, glob->data);
}

void mir_debug_print(MirCtx *ctx) {
    dynarray_foreach(ctx->globals, i) {
        mir_debug_glob(ctx, &ctx->globals[i]);
    }

    printf("\n");

    dynarray_foreach(ctx->fns, i) {
        mir_debug_fn(ctx, &ctx->fns[i]);
    }
}
