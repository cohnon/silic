#ifndef MIR_LOWER_H
#define MIR_LOWER_H

#include "compiler.h"
#include "mir/mir.h"


void mir_lower(Compiler *compiler);

#endif
