#ifndef MIR_H
#define MIR_H

#include <chnlib/chntype.h>
#include <chnlib/dynarray.h>
#include <chnlib/map.h>

typedef enum MirVarRefKind {
    MirVarRefKind_Invalid,
    MirVarRefKind_Void,
    MirVarRefKind_Glob,
    MirVarRefKind_Block,
    MirVarRefKind_VReg,
    MirVarRefKind_Const,
    MirVarRefKind_Fn,
} MirVarRefKind;

typedef struct MirVarRef {
    MirVarRefKind kind;
    u32           idx;
} MirVarRef;

typedef enum MirTypeKind {
    MirTypeKind_Invalid,
    MirTypeKind_Int,
    MirTypeKind_Float,
    MirTypeKind_Ptr,
    MirTypeKind_Fn,
} MirTypeKind;

typedef struct MirTypeFn {
    MirVarRef ret_type;
    DynArray(MirVarRef) arg_types;
} MirTypeFn;

typedef struct MirType {
    MirTypeKind kind;
    u8 bits;

    union {
        bool signed_;
        MirTypeFn fn;
    };
} MirType;

typedef struct MirTypeRef { u32 idx; } MirTypeRef;

typedef struct MirConst {
    MirTypeRef type;
    union {
        u64 int_;
    };
} MirConst;

typedef enum MirKind {

    // mem
    MirKind_Load,               // r0 = <ty> load r1
    MirKind_Store,              // r0 = <ty> store r1

    // terminators
    MirKind_Jmp,                // jmp <label>
    MirKind_Jeq,                // jeq <var> <label>
    MirKind_Jne,                // jne <var> <label>
    MirKind_Switch,             // switch(<ty> <var>) <label> | <const> <label> | ..
    MirKind_Ret,                // ret
    MirKind_Unreachable,        // unreachable

    // misc
    MirKind_Nop,
    MirKind_Set,                // r0 = set <ty> <var> <var> -- converted into ssa with phi functions
    MirKind_Copy,               // r0 = <ty> <var>
    MirKind_Call,               // r0 = <ty> @fn_name(<ty> <var>, <ty> <var>, ..)
    MirKind_Phi,                // r0 = <ty> phi <label> <var> | <label> <var> | ..

    // arithmetic
    MirKind_Add,                // r0 = <ty> add <var>, <var>
    MirKind_Sub,                // r0 = <ty> sub <var>, <var>
    MirKind_Mul,                // r0 = <ty> mul <var>, <var>
    MirKind_Div,                // r0 = <ty> div <var>, <var>
} MirKind;

typedef struct MirSwitchArgs {
    MirVarRef const_;
    MirVarRef label;
} MirSwitchArg;

typedef struct MirFnArg {
    MirTypeRef type;
    MirVarRef val;
} MirFnArg;

typedef struct MirFnCall {
    String name;
    DynArray(MirFnArg) args;
} MirFnCall;

typedef struct MirPhiArg {
    MirVarRef block;
    MirVarRef var;
} MirPhiArg;

typedef struct MirInstr MirInstr;
typedef struct MirInstr {
    u32 id;

    MirKind kind;
    MirTypeRef type;
    MirVarRef args[3];

    // special instructions
    union {
        DynArray(MirSwitchArg) switch_;
        MirFnCall              fn_call;
        DynArray(MirPhiArg)    phi;
        u64                    imm;
    };
} MirInstr;

// global
typedef enum MirLinkage {
    MirLinkage_Priv,
    MirLinkage_Pub,
} MirLinkage;

typedef struct MirGlob {
    String name;
    MirLinkage linkage;
    bool is_string_lit;

    u32 size;
    u8 *data;
} MirGlob;

// block
typedef struct MirBlock {
    MirVarRef fn;

    u32 id;
    bool terminated;
    bool seen; // move to ssa_pass

    DynArray(MirVarRef) phis;
    DynArray(MirVarRef) instrs;
    MirVarRef termi;

    DynArray(MirVarRef) preds;
    DynArray(MirVarRef) succs;
} MirBlock;

// fn
typedef struct MirFn {
    String name;
    MirLinkage linkage;

    MirVarRef entry;
    DynArray(MirVarRef) vars; // registers that are set (not in ssa)
    DynArray(MirVarRef) blocks;
    MirVarRef term;
    u32 instr_id;
} MirFn;

typedef struct MirCtx {
    DynArray(MirGlob)  globals;
    DynArray(MirConst) consts;
    DynArray(MirFn)    fns;
    DynArray(MirBlock) blocks;
    DynArray(MirInstr) instrs;

    DynArray(MirType)  types;

    struct {
        MirTypeRef invalid_ref;
        MirTypeRef i32_ref;
        MirTypeRef ptr_ref;
    } primitives;

    MirVarRef cur_fn;
    MirVarRef cur_block;
} MirCtx;

void mir_init(MirCtx *ctx);

MirVarRef mir_invalid(void);
bool mir_var_eq(MirVarRef a, MirVarRef b);

// types
MirTypeRef mir_type_invalid(MirCtx *ctx);
MirTypeRef mir_type_ptr(MirCtx *ctx);
MirTypeRef mir_type_int(MirCtx *ctx, u32 bits, bool is_signed);
MirTypeRef mir_type_float(MirCtx *ctx, u32 bits);

// fn
MirVarRef mir_fn(MirCtx *ctx, String name, MirLinkage linkage);

// block
MirVarRef mir_create_block(MirCtx *ctx);
void mir_insert_block(MirCtx *ctx, MirVarRef block);
void mir_use_block(MirCtx *ctx, MirVarRef block);
MirVarRef mir_cur_block(MirCtx *ctx);

// global
MirVarRef mir_glob(MirCtx *ctx, String name, MirTypeRef type, usize size, void *data, MirLinkage linkage, bool is_string_lit);
MirVarRef mir_string_lit(MirCtx *ctx, String val, MirLinkage linkage);

// const
MirVarRef mir_const(MirCtx *ctx, MirTypeRef type, u64 val);

// instr
MirVarRef mir_instr_add(MirCtx *ctx, MirTypeRef type, MirVarRef lhs, MirVarRef rhs);

void mir_instr_jeq(MirCtx *ctx, MirVarRef cond, MirVarRef then_block, MirVarRef else_block);
void mir_instr_jne(MirCtx *ctx, MirVarRef cond, MirVarRef then_block, MirVarRef else_block);
void mir_instr_jmp(MirCtx *ctx, MirVarRef block);
void mir_instr_ret(MirCtx *ctx, MirVarRef val);
void mir_instr_ret_void(MirCtx *ctx);
void mir_instr_unreachable(MirCtx *ctx);

MirVarRef mir_instr_set(MirCtx *ctx, MirVarRef lval, MirVarRef rval);
MirVarRef mir_instr_copy(MirCtx *ctx, MirVarRef var);
MirVarRef mir_instr_fn_call(MirCtx *ctx, String name, DynArray(MirFnArg) args);

MirVarRef mir_insert_phi(MirCtx *ctx, MirVarRef block_ref);
MirVarRef mir_insert_assosiated_phi(MirCtx *ctx, MirVarRef block_ref, MirVarRef var);
void mir_phi_add_incoming(MirCtx *ctx, MirVarRef phi, MirVarRef var, MirVarRef from);

void mir_debug_print(MirCtx *ctx);

#endif
