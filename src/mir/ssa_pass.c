#include "ssa_pass.h"

#include <chnlib/dynarray.h>
#include <stdio.h>
#include <assert.h>


typedef struct SsaPassCtx {
    MirCtx *mir;
    MirFn  *cur_fn;

    DynArray(MirVarRef) *dom_front;
    MirVarRef           *idoms;
} SsaPassCtx;


// A Simple, Fast Dominance Algorithm
// Authors: Keith D. Cooper, Timothy J. Harvey, Ken Kennedy
// https://www.researchgate.net/publication/2569680_A_Simple_Fast_Dominance_Algorithm
static MirVarRef intersect(SsaPassCtx *ctx, MirVarRef b1, MirVarRef b2) {
    MirCtx *mir = ctx->mir;

    MirVarRef finger1 = b1;
    MirVarRef finger2 = b2;

    while (true) {
        u32 id1 = mir->blocks[finger1.idx].id;
        u32 id2 = mir->blocks[finger2.idx].id;

        if (id1 == id2) {
            break;
        }

        if (id1 > id2) {
            finger1 = ctx->idoms[id1];
        }

        if (id2 > id1) {
            finger2 = ctx->idoms[id2];
        }
    }

    return finger1;
}

static void gen_idom(SsaPassCtx *ctx) {
    MirCtx *mir = ctx->mir;
    MirFn *cur_fn = ctx->cur_fn;

    usize n_blocks = dynarray_len(cur_fn->blocks);
    ctx->idoms = malloc(sizeof(i32) * n_blocks);

    for (usize i = 0; i < n_blocks; i += 1) {
        ctx->idoms[i].kind = MirVarRefKind_Invalid;
    }

    ctx->idoms[0] = cur_fn->blocks[0];

    bool changed = true;
    while (changed) {
        changed = false;

        for (usize i = 1; i < dynarray_len(cur_fn->blocks); i += 1) {
            MirVarRef block_ref = cur_fn->blocks[i];
            MirBlock *block = &mir->blocks[block_ref.idx];

            if (dynarray_len(block->preds) == 0) {
                continue;
            }

            // pick first non null pred
            usize pred_i = 0;
            MirVarRef new_idom = block->preds[pred_i];
            u32 new_idom_id = mir->blocks[new_idom.idx].id;
            while (ctx->idoms[new_idom_id].kind == MirVarRefKind_Invalid) {
                pred_i += 1;
                new_idom = block->preds[pred_i];
                new_idom_id = mir->blocks[new_idom.idx].id;
            }

            assert(pred_i < n_blocks);

            pred_i += 1;
            for (; pred_i < dynarray_len(block->preds); pred_i += 1) {
                MirVarRef pred = block->preds[pred_i];
                if (ctx->idoms[mir->blocks[pred.idx].id].kind == MirVarRefKind_Invalid) {
                    continue;
                }

                new_idom = intersect(ctx, new_idom, pred);
            }

            if (ctx->idoms[block->id].kind == MirVarRefKind_Invalid
            or (ctx->idoms[block->id].idx != new_idom.idx)) {
                ctx->idoms[block->id] = new_idom;
                changed = true;
            }
        }
    }
}

static void gen_dom_front(SsaPassCtx *ctx) {
    MirCtx *mir = ctx->mir;
    MirFn *cur_fn = ctx->cur_fn;

    dynarray_foreach(cur_fn->blocks, i) {
        MirVarRef block_ref = cur_fn->blocks[i];
        MirBlock *block = &mir->blocks[block_ref.idx];
        if (dynarray_len(block->preds) < 2) {
            continue;
        }

        dynarray_foreach(block->preds, j) {
            MirVarRef pred = block->preds[j];

            MirVarRef runner = pred;
            while (runner.idx != ctx->idoms[block->id].idx) {
                dynarray_push(ctx->dom_front[mir->blocks[runner.idx].id], &block_ref);
                runner = ctx->idoms[mir->blocks[runner.idx].id];
            }
        }
    }
}

static void insert_phi_nodes(SsaPassCtx *ctx) {
    MirCtx *mir = ctx->mir;
    MirFn *cur_fn = ctx->cur_fn;

    
    // clear seen blocks
    dynarray_foreach(cur_fn->blocks, block_i) {
        MirVarRef block_ref = cur_fn->blocks[block_i];
        MirBlock *block = &mir->blocks[block_ref.idx];
        block->seen = false;
    }

    dynarray_foreach(cur_fn->vars, var_i) {
        MirVarRef var_ref = cur_fn->vars[var_i];
        DynArray(MirVarRef) def_blocks = dynarray_init();

        // get iterated dominance frontier
        
        // insert variable definitions
        dynarray_foreach(cur_fn->blocks, block_i) {
            MirVarRef def_block_ref = cur_fn->blocks[block_i];
            MirBlock *block = &mir->blocks[def_block_ref.idx];

            dynarray_foreach(block->instrs, instr_i) {
                MirVarRef instr_ref = block->instrs[instr_i];
                MirInstr *instr = &mir->instrs[instr_ref.idx];
                if (instr->kind == MirKind_Set and mir_var_eq(instr->args[0], var_ref)) {
                    dynarray_push(def_blocks, &def_block_ref);
                }
            }
        }

        // get frontiers of definitions (inlcuding phis also need to be added to def_blocks)
        for (usize def_i = 0; def_i < dynarray_len(def_blocks); def_i += 1) {
            MirVarRef def_ref = def_blocks[def_i];
            MirBlock *def_block = &mir->blocks[def_ref.idx];

            dynarray_foreach(ctx->dom_front[def_block->id], front_i) {
                MirVarRef phi_block_ref = ctx->dom_front[def_block->id][front_i];
                MirBlock *phi_block = &mir->blocks[phi_block_ref.idx];


                if (not phi_block->seen) {
                    phi_block->seen = true;

                    mir_insert_assosiated_phi(mir, phi_block_ref, var_ref);
                    dynarray_push(def_blocks, &phi_block_ref);
                }
            }
        }
    }
}

typedef struct VarBlockPair {
    MirVarRef var;
    MirVarRef block;
} VarBlockPair;

static VarBlockPair var_stack_top(DynArray(VarBlockPair) var_stack) {
    return dynarray_last(var_stack);
}

static void rename_reg_rec(SsaPassCtx *ctx, MirVarRef block_ref, MirVarRef var_ref, DynArray(VarBlockPair) *var_stack) {
    MirCtx *mir = ctx->mir;

    MirBlock *block = &mir->blocks[block_ref.idx];

    dynarray_foreach(block->instrs, i) {
        MirVarRef instr_ref = block->instrs[i];
        MirInstr *instr = &mir->instrs[instr_ref.idx];

        switch (instr->kind) {
        case MirKind_Set: {
            if (mir_var_eq(instr->args[0], var_ref)) {
                VarBlockPair pair = (VarBlockPair){ instr_ref, block_ref };
                instr->kind = MirKind_Copy;
                instr->args[0] = instr->args[1];
                dynarray_push(*var_stack, &pair);
            }

            break;
        }

        case MirKind_Call: {
            dynarray_foreach(instr->fn_call.args, i) {
                MirFnArg *arg = &instr->fn_call.args[i];
                if (mir_var_eq(arg->val, var_ref)) {
                    arg->val = var_stack_top(*var_stack).var;
                }
            }

            break;
        }

        default: {
            if (mir_var_eq(instr->args[0], var_ref)) {
                instr->args[0] = var_stack_top(*var_stack).var;
            }

            if (mir_var_eq(instr->args[1], var_ref)) {
                instr->args[1] = var_stack_top(*var_stack).var;
            }

            if (mir_var_eq(instr->args[3], var_ref)) {
                instr->args[3] = var_stack_top(*var_stack).var;
            }
            
            break;
        }

        }

    }

    // update phis
    dynarray_foreach(block->succs, succ_i) {
        MirVarRef succ_ref = block->succs[succ_i];
        MirBlock *succ = &mir->blocks[succ_ref.idx];

        dynarray_foreach(succ->phis, phi_i) {
            MirVarRef phi = succ->phis[phi_i];
            MirInstr *instr = &mir->instrs[phi.idx];

            if (mir_var_eq(instr->args[0], var_ref)) {
                mir_phi_add_incoming(mir, phi, var_stack_top(*var_stack).var, block_ref);
                break;
            }
        }
    }

    dynarray_foreach(block->succs, i) {
        MirVarRef succ_ref = block->succs[i];
        rename_reg_rec(ctx, succ_ref, var_ref, var_stack);
    }
}

static void rename_regs(SsaPassCtx *ctx) {
    MirCtx *mir = ctx->mir;
    MirFn *cur_fn = ctx->cur_fn;

    DynArray(VarBlockPair) var_stack = dynarray_init();

    dynarray_foreach(cur_fn->vars, var_i) {
        MirVarRef var_ref = cur_fn->vars[var_i];

        // clear seen blocks
        dynarray_foreach(cur_fn->blocks, block_i) {
            MirVarRef block_ref = cur_fn->blocks[block_i];
            MirBlock *block = &mir->blocks[block_ref.idx];
            block->seen = false;
        }

        rename_reg_rec(ctx, cur_fn->entry, var_ref, &var_stack);
    }
}

void mir_ssa_pass(MirCtx *mir) {
    SsaPassCtx ctx;
    ctx.mir = mir;

    dynarray_foreach(mir->fns, i) {
        MirFn *fn = &mir->fns[i];
        ctx.cur_fn = fn;

        ctx.dom_front = malloc(sizeof(DynArray(u32)) * dynarray_len(fn->blocks));
        for (usize i = 0; i < dynarray_len(fn->blocks); i += 1) {
            ctx.dom_front[i] = dynarray_init();
        }

        gen_idom(&ctx);

        /*
        printf("idoms: ");
        for (usize i = 0; i < dynarray_len(fn->blocks); i += 1) {
            printf("%d ", mir->blocks[ctx.idoms[i].idx].id);
        }
        printf("\n");
        */

        gen_dom_front(&ctx);

        /*
        printf("\n%.*s domf:\n", str_format(fn->name));
        for (usize i = 0; i < dynarray_len(fn->blocks); i += 1) {
            MirVarRef block = fn->blocks[i];
            printf(".b%d: ", mir->blocks[block.idx].id);
            dynarray_foreach(ctx.dom_front[i], j) {
                printf(".b%d ", mir->blocks[ctx.dom_front[i][j].idx].id);
            }
            printf("\n");
        }
        printf("\n");
        */

        insert_phi_nodes(&ctx);

        rename_regs(&ctx);

        free(ctx.idoms);
    }
}
