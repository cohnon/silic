#include "lexer.h"

#include "parse/token.h"

#include <chnlib/logger.h>
#include <assert.h>


#define WHITESPACE \
    ' ': case '\t': case '\n': case '\v': case '\f': case '\r'

#define DIGIT \
    '0': case '1': case '2': case '3': case '4': \
    case '5': case '6': case '7': case '8': case '9'

#define ALPHA \
    'a': case 'b': case 'c': case 'd': case 'e': case 'f': case 'g': \
    case 'h': case 'i': case 'j': case 'k': case 'l': case 'm': case 'n': \
    case 'o': case 'p': case 'q': case 'r': case 's': case 't': case 'u': \
    case 'v': case 'w': case 'x': case 'y': case 'z': case 'A': case 'B': \
    case 'C': case 'D': case 'E': case 'F': case 'G': case 'H': case 'I': \
    case 'J': case 'K': case 'L': case 'M': case 'N': case 'O': case 'P': \
    case 'Q': case 'R': case 'S': case 'T': case 'U': case 'V': case 'W': \
    case 'X': case 'Y': case 'Z'


typedef struct LexerCtx {
    Module *module;
    String src;
    usize src_idx;
    Token *current_token;
    TextPosition position;
} LexerCtx;

static char cur_char(LexerCtx *ctx) {
    return ctx->src.ptr[ctx->src_idx];
}

static char next_char(LexerCtx *ctx) {
    return ctx->src.ptr[ctx->src_idx + 1];
}

static void inc(LexerCtx *ctx) {
    ctx->src_idx += 1;
    ctx->position.column += 1;
}

static void inc2(LexerCtx *ctx) {
    ctx->src_idx += 2;
    ctx->position.column += 2;
}

static void begin_token(LexerCtx *ctx, TokenKind kind) {
    Token *token = dynarray_add(ctx->module->token_list);

    token->kind = kind;
    token->position = ctx->position;
    token->span.ptr = &ctx->module->src.ptr[ctx->src_idx];

    ctx->current_token = token;
}

typedef struct SymTok { char *symbol; TokenKind token; } SymTok;
const SymTok sym_tok_map[] = {
    { "use",         TokenKind_KeywordUse         },
    { "unreachable", TokenKind_KeywordUnreachable },
    { "as",          TokenKind_KeywordAs          },
    { "asm",         TokenKind_KeywordAsm         },
    { "volatile",    TokenKind_KeywordVolatile    },
    { "fn",          TokenKind_KeywordFn          },
    { "ret",         TokenKind_KeywordRet         },
    { "let",         TokenKind_KeywordLet         },
    { "extern",      TokenKind_KeywordExtern      },
    { "if",          TokenKind_KeywordIf          },
    { "match",       TokenKind_KeywordSwitch      },
    { "else",        TokenKind_KeywordElse        },
    { "true",        TokenKind_KeywordTrue        },
    { "false",       TokenKind_KeywordFalse       },
    { "type",        TokenKind_KeywordSwitch      },
    { "pub",         TokenKind_KeywordPub         },
    { "const",       TokenKind_KeywordConst       },
    { "loop",        TokenKind_KeywordLoop        },
    { "break",       TokenKind_KeywordBreak       },
    { "continue",    TokenKind_KeywordContinue    },
    { "and",         TokenKind_KeywordAnd         },
    { "or",          TokenKind_KeywordOr          },
    { "not",         TokenKind_KeywordNot         },
    { "mut",         TokenKind_KeywordMut         },
};

static void end_token(LexerCtx *ctx) {
    Token *token = ctx->current_token;
    usize start_idx = token->span.ptr - ctx->module->src.ptr;
    token->span.len = ctx->src_idx - start_idx;

    if (token->kind == TokenKind_Symbol) {
        for (usize i = 0; i < sizeof(sym_tok_map) / sizeof(sym_tok_map[0]); i += 1) {
            SymTok sym_tok = sym_tok_map[i];
            if (token_compare_literal(token, sym_tok.symbol)) {
                token->kind = sym_tok.token;
                break;
            }
        }
    }
}

static void skip_whitespace(LexerCtx *ctx) {
    assert(cur_char(ctx) <= ' ');

    while (ctx->src_idx < ctx->src.len and cur_char(ctx) <= ' ') {
        if (cur_char(ctx) == '\n') {
            ctx->position.line += 1;
            // set to 0 since we inc right after
            ctx->position.column = 0;
        }

        inc(ctx);
    }
}

static void lex_symbol(LexerCtx *ctx) {
    assert(('a' <= cur_char(ctx) and cur_char(ctx) <= 'z')
    or     ('A' <= cur_char(ctx) and cur_char(ctx) <= 'Z')
    or     (cur_char(ctx) == '_'));

    begin_token(ctx, TokenKind_Symbol);

    inc(ctx);

    while (('a' <= cur_char(ctx) and cur_char(ctx) <= 'z')
    or     ('A' <= cur_char(ctx) and cur_char(ctx) <= 'Z')
    or     ('0' <= cur_char(ctx) and cur_char(ctx) <= '9')
    or     (cur_char(ctx) == '_')) {
        inc(ctx);
    }

    end_token(ctx);
}

static void lex_number(LexerCtx *ctx) {
    assert('0' <= cur_char(ctx) and cur_char(ctx) <= '9');

    begin_token(ctx, TokenKind_NumberLit);

    if (cur_char(ctx) == '0' and next_char(ctx) == 'b') {
        inc2(ctx);

        while (('0' <= cur_char(ctx) and cur_char(ctx) <= '1')
        or     cur_char(ctx) == '_') {
            inc(ctx);
        }

    } else if (cur_char(ctx) == '0' and next_char(ctx) == 'o') {
        inc2(ctx);

        while (('0' <= cur_char(ctx) and cur_char(ctx) <= '7')
        or     cur_char(ctx) == '_') {
            inc(ctx);
        }

    } else if (cur_char(ctx) == '0' and next_char(ctx) == 'x') {
        inc2(ctx);

        while (('0' <= cur_char(ctx) and cur_char(ctx) <= '9')
        or     ('a' <= cur_char(ctx) and cur_char(ctx) <= 'f')
        or     ('A' <= cur_char(ctx) and cur_char(ctx) <= 'F')
        or      cur_char(ctx) == '_') {
            inc(ctx);
        }
    } else {
        inc(ctx);

        bool seen_decimal_point = false;

        while (true) {
            if (not seen_decimal_point and cur_char(ctx) == '.') {
                seen_decimal_point = true;
                inc(ctx);
                continue;
            }

            if ('0' <= cur_char(ctx) and cur_char(ctx) <= '9'
            or   cur_char(ctx) == '_') {
                inc(ctx);
                continue;
            }

            break;
        }
    }

    end_token(ctx);
}

static void lex_string(LexerCtx *ctx) {
    assert(cur_char(ctx) == '"');

    begin_token(ctx, TokenKind_StringLit);

    inc(ctx);

    while (cur_char(ctx) != '"') {
        inc(ctx);
    }

    inc(ctx); // eat '"'

    end_token(ctx);
}

static void lex_dash(LexerCtx *ctx) {
    assert(cur_char(ctx) == '-');

    begin_token(ctx, TokenKind_Dash);

    inc(ctx);

    if (cur_char(ctx) == '>') {
        inc(ctx);

        ctx->current_token->kind = TokenKind_Arrow;
        end_token(ctx);

        return;
    }

    end_token(ctx);
}

static void lex_comment(LexerCtx *ctx) {
    assert(cur_char(ctx) == '/' or cur_char(ctx) == '*');

    ctx->current_token->kind = TokenKind_StringLit;

    if (cur_char(ctx) == '/') {
        while (cur_char(ctx) != '\n') {
            inc(ctx);
        }

        return;
    }

    // nested comment
    inc(ctx); // eat '*'

    u32 nested_lvl = 1;

    while (nested_lvl > 0) {
        if (cur_char(ctx) == '/' and next_char(ctx) == '*') {
            nested_lvl += 1;
            inc2(ctx);
        
        } else if (cur_char(ctx) == '*' and next_char(ctx) == '/') {
            nested_lvl -= 1;
            inc2(ctx);
        
        } else {
            inc(ctx);
        }
    }

    end_token(ctx);
}

static void lex_slash(LexerCtx *ctx) {
    assert(cur_char(ctx) == '/');

    begin_token(ctx, TokenKind_Slash);

    inc(ctx);

    if (cur_char(ctx) == '/' or cur_char(ctx) == '*') {
        lex_comment(ctx);
        return;
    }

    end_token(ctx);
}

void lex_module(Module *module) {
    LexerCtx ctx;
    ctx.module = module;
    ctx.src = module->src;
    ctx.src_idx = 0;
    ctx.current_token = null;
    ctx.position = (TextPosition){ 1, 1};

    while (ctx.src_idx < ctx.src.len) {
        #define CASE_SINGLE_CHAR(ch, kind) case ch: \
            begin_token(&ctx, kind);                \
            inc(&ctx);                              \
            end_token(&ctx);                        \
            break

        switch (cur_char(&ctx)) {
        case WHITESPACE:      skip_whitespace(&ctx); break;
        case ALPHA: case '_': lex_symbol(&ctx); break;
        case DIGIT:           lex_number(&ctx); break;
        case '"':             lex_string(&ctx); break;
        case '/':             lex_slash(&ctx); break;
        case '-':             lex_dash(&ctx); break;

        CASE_SINGLE_CHAR(';', TokenKind_Semicolon);
        CASE_SINGLE_CHAR(',', TokenKind_Comma);
        CASE_SINGLE_CHAR('{', TokenKind_LBrace);
        CASE_SINGLE_CHAR('}', TokenKind_RBrace);
        CASE_SINGLE_CHAR('(', TokenKind_LParen);
        CASE_SINGLE_CHAR(')', TokenKind_RParen);
        CASE_SINGLE_CHAR('!', TokenKind_Bang);
        CASE_SINGLE_CHAR(':', TokenKind_Colon);

        CASE_SINGLE_CHAR('.', TokenKind_Dot);
        CASE_SINGLE_CHAR('&', TokenKind_Ampersand);
        CASE_SINGLE_CHAR('%', TokenKind_Percent);
        CASE_SINGLE_CHAR('+', TokenKind_Plus);
        CASE_SINGLE_CHAR('=', TokenKind_Equals);
        CASE_SINGLE_CHAR('>', TokenKind_GreaterThan);
        CASE_SINGLE_CHAR('<', TokenKind_LessThan);
        CASE_SINGLE_CHAR('~', TokenKind_Tilde);
        CASE_SINGLE_CHAR('*', TokenKind_Star);

        default: {
            char unknown_char = cur_char(&ctx);
            begin_token(&ctx, TokenKind_Invalid);
            inc(&ctx);
            end_token(&ctx);
            errors_new(
                &ctx.module->errors,
                ctx.module->path,
                ctx.current_token->position,
                ctx.current_token->span,
                "unexpected character '%c'",
                unknown_char
            );
            return;
        }

        }
    }

    begin_token(&ctx, TokenKind_Eof);
    end_token(&ctx);
}
