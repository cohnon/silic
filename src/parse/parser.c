#include "parser.h"

#include "module.h"
#include "parse/token.h"

#include <chnlib/maybe.h>
#include <chnlib/logger.h>
#include <chnlib/dynarray.h>
#include <stdio.h>
#include <stdlib.h>


typedef struct ParserCtx {
    Module *module;
    bool in_loop;
    unsigned int token_index;
} ParserCtx;

static Token* current_token(ParserCtx *ctx) {
    return &ctx->module->token_list[ctx->token_index];
}

static Token* consume_token(ParserCtx *ctx) {
    Token *token = current_token(ctx);
    ctx->token_index += 1;

    return token;
}

static Token* expect_token(ParserCtx *ctx, TokenKind kind) {
    Token *token = consume_token(ctx);
    if (token->kind != kind) {
        errors_new(
            &ctx->module->errors,
            ctx->module->path,
            token->position,
            token->span,
            "expected %s, found %s",
            token_string(kind),
            token_string(token->kind)
        );

        return null;
    }

    return token;
}

static bool expect_semicolon(ParserCtx *ctx) {
    Token *token = current_token(ctx);
    if (token->kind != TokenKind_Semicolon) {
        // add the semicolon token for error
        Token *prev_token = &ctx->module->token_list[ctx->token_index - 1];

        TextPosition pos = prev_token->position;
        pos.column += prev_token->span.len;

        String span = prev_token->span;
        span.ptr += prev_token->span.len;
        span.len = 1;

        errmsg_id err = errors_new(
            &ctx->module->errors,
            ctx->module->path,
            pos, span,
            "expected ';'"
        );
        errors_add_hint(ctx->module->errors, err, "you fool");

        return false;
    }

    consume_token(ctx);

    return true;
}

static bool consume_if(ParserCtx *ctx, TokenKind kind) {
    Token *token = current_token(ctx);
    if (token->kind == kind) {
        consume_token(ctx);

        return true;
    }

    return false;
}

static Item* parse_item(ParserCtx *ctx);
static Stmt* parse_stmt(ParserCtx *ctx);
static Expr* parse_expr(ParserCtx *ctx, bool *parsed);

static bool parse_modpath(ParserCtx *ctx, ModPath *modpath) {
    modpath->path = dynarray_init();

    while (true) {
        Token *token = try(expect_token(ctx, TokenKind_Symbol));
        dynarray_push(modpath->path, &token->span);
        
        if (consume_if(ctx, TokenKind_Colon)) {
            continue;
        }

        break;
    }

    return true;
}

static Ast_Type* parse_type(ParserCtx *ctx) {
    Ast_Type *type = malloc(sizeof(Ast_Type));
    
    Token *token = current_token(ctx);

    switch (token->kind) {
        case TokenKind_Star: {
            consume_token(ctx);

            type->kind = AstTypeKind_Ptr;
                if (current_token(ctx)->kind == TokenKind_KeywordMut) {
                    consume_token(ctx);
                    type->ptr.is_mut = true;
                } else {
                    type->ptr.is_mut = false;
                }
            type->ptr.to = try(parse_type(ctx));

            break;
        }

        case TokenKind_KeywordUnreachable: {
            consume_token(ctx);

            type->kind = AstTypeKind_Never;

            break;
        }

        case TokenKind_Symbol: {
            type->kind = AstTypeKind_Symbol;
            try(parse_modpath(ctx, &type->symbol.path));

            break;
        }

        default: {
            errors_new(
                &ctx->module->errors,
                ctx->module->path,
                token->position,
                token->span,
                "unexpected type: %s", token_string(token->kind)
            );

            return null;
        }
    }

    return type;
}

static void operator_precedence(TokenKind operator_kind, int *left, int *right) {
    int precedence;
    switch (operator_kind) {
        case TokenKind_KeywordAnd: precedence = OpPrec_And; break;
        case TokenKind_KeywordOr: precedence = OpPrec_Or; break;
        case TokenKind_GreaterThan: precedence = OpPrec_CmpGt; break;
        case TokenKind_LessThan: precedence = OpPrec_CmpLt; break;
        case TokenKind_Equality: precedence = OpPrec_CmpEq; break;
        case TokenKind_Inequality: precedence = OpPrec_CmpNotEq; break;
        case TokenKind_Equals: precedence = OpPrec_Assign; break;
        case TokenKind_Plus: precedence = OpPrec_Add; break;
        case TokenKind_Dash: precedence = OpPrec_Sub; break;
        case TokenKind_Star: precedence = OpPrec_Mul; break;
        case TokenKind_Slash: precedence = OpPrec_Div; break;
        default: precedence = OpPrec_Invalid;
    }

    *left = (precedence  *2) - 1;
    *right = precedence  *2;
}

static bool parse_block(ParserCtx *ctx, Expr *expr) {
    Block *block = &expr->block;
    block->stmts = dynarray_init();

    try(expect_token(ctx, TokenKind_LBrace));
    
    while (current_token(ctx)->kind != TokenKind_RBrace) {
        Stmt *stmt = try(parse_stmt(ctx));
        dynarray_push(block->stmts, &stmt);

        // an expression without a semi and isn't a control statement should be at the end of a block
        if (stmt->kind == StmtKind_NakedExpr and not should_remove_stmt_semi(stmt->expr)) {
            // if we're not at a '}', then we exited early and there should be a semi
            if (current_token(ctx)->kind != TokenKind_RBrace) {
                try(expect_semicolon(ctx));
            }
            break;
        }
    }

    consume_token(ctx);

    return true;
}

static bool parse_number_literal(ParserCtx *ctx, Expr *expr) {
    NumberLit *lit = &expr->number_literal;

    Token *token = consume_token(ctx);
    lit->span = token->span;
    lit->int_ = 0;

    if (lit->span.ptr[0] == '0') {
        if (lit->span.ptr[1] == 'b') {
            for (usize i = 2; i < lit->span.len; i += 1) {
                u8 bit = lit->span.ptr[i] - '0';
                lit->int_ = (lit->int_ << 1) | bit;
            }

            return true;
        }

        if (lit->span.ptr[1] == 'o') {
            for (usize i = 2; i < lit->span.len; i += 1) {
                u8 bits = lit->span.ptr[i] - '0';
                lit->int_ = (lit->int_ << 3) | bits;
            }

            return true;
        }

        if (lit->span.ptr[1] == 'x') {
            for (usize i = 2; i < lit->span.len; i += 1) {
                char cur = lit->span.ptr[i];
                u8 nibble;
                if ('0' <= cur and cur <= '9')      nibble = cur - '0';
                else if ('a' <= cur and cur <= 'f') nibble = cur - 'a' + 10;
                else if ('A' <= cur and cur <= 'F') nibble = cur - 'A' + 10;
                else __builtin_unreachable();

                lit->int_ = (lit->int_ << 4) | nibble;
            }

            return true;
        }
    }

    bool seen_decimal_point = false;
    u32 nth_pos = 0;
    u64 int_ = 0;
    f64 float_ = 0.0f;
    for (usize i = 0; i < lit->span.len; i += 1) {
        if (lit->span.ptr[i] == '.') {
            float_ = int_;

            seen_decimal_point = true;
            continue;
        }

        if (seen_decimal_point) {
            nth_pos += 1;

            f64 val = (f64)(lit->span.ptr[i] - '0') / (10.0f * nth_pos);
            float_ += val;

        } else {
            int_ *= 10;
            int_ += lit->span.ptr[i] - '0';
        }
    }

    if (seen_decimal_point) lit->float_ = float_;
    else                    lit->int_ = int_;

    return true;
}

// TODO: maybe make this part of block
static bool parse_asmblock(ParserCtx *ctx, Expr *expr) {
    Asm *asm = &expr->asm;
    asm->inputs = dynarray_init();
    asm->outputs = dynarray_init();
    asm->clobbers = dynarray_init();
    asm->source = dynarray_init();

    // consume 'asm'
    consume_token(ctx);

    try(expect_token(ctx, TokenKind_KeywordVolatile));

    if (current_token(ctx)->kind == TokenKind_LParen) {
        consume_token(ctx);

        while (current_token(ctx)->kind != TokenKind_RParen) {
            Token *reg_tok = try(expect_token(ctx, TokenKind_Symbol));

            if (current_token(ctx)->kind == TokenKind_Equals) {
                consume_token(ctx);

                AsmInput *param = dynarray_add(asm->inputs);
                param->reg = reg_tok->span;

                param->val = try(parse_expr(ctx, null));
            } else {
                String *clobber = dynarray_add(asm->clobbers);
                *clobber = reg_tok->span;
            }

            if (current_token(ctx)->kind != TokenKind_Comma) {
                break;
            }

            // consume ','
            consume_token(ctx);
        }

        // consume ')'
        consume_token(ctx);
    }

    if (current_token(ctx)->kind == TokenKind_Arrow) {
        consume_token(ctx);

        Token *reg_tok = try(expect_token(ctx, TokenKind_Symbol));
        String *output = dynarray_add(asm->outputs);

        *output = reg_tok->span;
    }

    try(expect_token(ctx, TokenKind_LBrace));

    while (current_token(ctx)->kind != TokenKind_RBrace) {
        Token *line_tok = try(expect_token(ctx, TokenKind_StringLit));
        StringLit *line = dynarray_add(asm->source);
        line->span = line_tok->span;
    }

    // '}'
    consume_token(ctx);

    return true;
}

static Expr* parse_primary_expr(ParserCtx *ctx, bool *parsed) {
    Expr *expr = malloc(sizeof(Expr));
    expr->src_pos = current_token(ctx)->position;
    expr->span = current_token(ctx)->span;

    if (parsed != null) {
        *parsed = true;
    }

    switch (current_token(ctx)->kind) {
        case TokenKind_KeywordAsm: {
            expr->kind = ExprKind_Asm;
            try(parse_asmblock(ctx, expr));

            break;
        }

        case TokenKind_LParen: {
            consume_token(ctx);
            expr = try(parse_expr(ctx, null));

            try(expect_token(ctx, TokenKind_RParen));

            break;
        }

        case TokenKind_Dash: {
            // TODO make unary precedence
            consume_token(ctx);

            expr->kind = ExprKind_UnOp;
            expr->unary_op.kind = UnOpKind_Neg;
            expr->unary_op.expr = try(parse_primary_expr(ctx, null));

            break;
        }

        case TokenKind_Ampersand: {
            consume_token(ctx);

            expr->kind = ExprKind_UnOp;
            expr->unary_op.kind = UnOpKind_AddrOf;
            expr->unary_op.expr = try(parse_expr(ctx, null));

            break;
        }

        case TokenKind_KeywordRet: {
            expr->kind = ExprKind_Ret;
            consume_token(ctx);

            bool parsed;
            expr->ret.expr = parse_expr(ctx, &parsed);

            // expr exists but errored
            if (parsed and expr->ret.expr == null) {
                return null;
            }

            break;
        }

        case TokenKind_NumberLit: {
            expr->kind = ExprKind_NumberLit;
            try(parse_number_literal(ctx, expr));

            break;
        }

        case TokenKind_StringLit: {
            expr->kind = ExprKind_StringLit;
            expr->string_literal.span = consume_token(ctx)->span;

            break;
        }

        case TokenKind_KeywordTrue:
        case TokenKind_KeywordFalse: {
            Token *boolean = consume_token(ctx);

            expr->kind = ExprKind_BoolLit;
            expr->boolean = boolean->kind == TokenKind_KeywordTrue;

            break;
        }

        case TokenKind_KeywordLet: {
            consume_token(ctx);

            expr->kind = ExprKind_Let;

            Token *name_token = try(expect_token(ctx, TokenKind_Symbol));
            expr->let.name = name_token->span;

                Token *maybe_colon = current_token(ctx);
                if (maybe_colon->kind == TokenKind_Colon) {
                    consume_token(ctx);
                    expr->let.type = try(parse_type(ctx));
                } else {
                    expr->let.type = null;
                }

            try(expect_token(ctx, TokenKind_Equals));

            expr->let.value = try(parse_expr(ctx, null));

            break;
        }

        case TokenKind_Symbol: {
            expr->kind = ExprKind_Symbol;

            try(parse_modpath(ctx, &expr->symbol.path));

            // fn call
            if (current_token(ctx)->kind == TokenKind_LParen) {
                consume_token(ctx);

                expr->kind = ExprKind_FnCall;

                expr->fn_call.modpath = expr->symbol.path;
                expr->fn_call.arguments = dynarray_init();

                while (current_token(ctx)->kind != TokenKind_RParen) {
                    Expr *arg = try(parse_expr(ctx, null));
                    dynarray_push(expr->fn_call.arguments, &arg);

                    if (current_token(ctx)->kind != TokenKind_Comma) {
                        break;
                    }

                    consume_token(ctx);
                }

                try(expect_token(ctx, TokenKind_RParen));

                break;
            }


            break;
        }

        case TokenKind_LBrace: {
            expr->kind = ExprKind_Block;
            try(parse_block(ctx, expr));

            break;
        }

        case TokenKind_KeywordBreak: {
            consume_token(ctx);
            expr->kind = ExprKind_Break;
            break;
        }

        case TokenKind_KeywordContinue: {
            consume_token(ctx);
            expr->kind = ExprKind_Continue;
            break;
        }

        case TokenKind_KeywordUnreachable: {
            consume_token(ctx);
            expr->kind = ExprKind_Unreachable;
            break;
        }

        case TokenKind_KeywordIf: {
            consume_token(ctx);
            expr->kind = ExprKind_If;
            expr->if_expr.condition = try(parse_expr(ctx, null));

            if (current_token(ctx)->kind != TokenKind_LBrace) {
                chn_error("Expected block after if");
            }
            expr->if_expr.then = try(parse_expr(ctx, null));
           
            // else (if) branch
            if (current_token(ctx)->kind == TokenKind_KeywordElse) {
                consume_token(ctx);

                Token *next_token = current_token(ctx);
                if (next_token->kind != TokenKind_KeywordIf &&
                    next_token->kind != TokenKind_LBrace
                ) {
                    chn_error("Expected 'if' or '{' after an else");
                }

                expr->if_expr.otherwise = try(parse_expr(ctx, null));

            } else {
                expr->if_expr.otherwise = null;
            }

            break;
        }

        case TokenKind_KeywordSwitch: {
            consume_token(ctx);
            expr->kind = ExprKind_Switch;
            expr->switch_expr.branches = dynarray_init();

            expr->switch_expr.condition = try(parse_expr(ctx, null));

            try(expect_token(ctx, TokenKind_LBrace));
           
            while (current_token(ctx)->kind != TokenKind_RBrace) {
                SwitchBranch branch;
                branch.value = try(parse_expr(ctx, null));

                try(expect_token(ctx, TokenKind_Arrow));
                branch.then = try(parse_expr(ctx, null));

                dynarray_push(expr->switch_expr.branches, &branch);

                if (current_token(ctx)->kind != TokenKind_RBrace) {
                    try(expect_token(ctx, TokenKind_Comma));
                }
            }

            try(expect_token(ctx, TokenKind_RBrace));

            break;
        }

        case TokenKind_KeywordLoop: {
            consume_token(ctx);
            expr->kind = ExprKind_Loop;

            // TODO: make parse_block_expr
            if (current_token(ctx)->kind != TokenKind_LBrace) {
                Token *token = current_token(ctx);
                errmsg_id err = errors_new(
                    &ctx->module->errors,
                    ctx->module->path,
                    token->position,
                    token->span,
                    "loop body must be a block"
                );
                errors_add_hint(ctx->module->errors, err, "expected '{'");

                return null;
            }

            if (current_token(ctx)->kind != TokenKind_LBrace) {
                chn_error("Expected block after loop");
            }
            expr->loop.body = try(parse_primary_expr(ctx, null));

            break;
        }

        default: {
            if (parsed != null) {
                *parsed = false;

            } else {
                Token *token = current_token(ctx);
                errors_new(
                    &ctx->module->errors,
                    ctx->module->path,
                    token->position,
                    token->span,
                    "expected expression."
                );
            }

            return null;
        }
    }

    return expr;
}

static Expr* parse_expr_prec(ParserCtx *ctx, bool *parsed, int precedence) {
    Expr *left_expression = try(parse_primary_expr(ctx, parsed));

    while (1) {
        Token *operator_token = current_token(ctx);

        // special cases
        // casting
        if (operator_token->kind == TokenKind_KeywordAs) {
            consume_token(ctx);

            Expr *cast = malloc(sizeof(Expr));
            cast->kind = ExprKind_Cast;
            cast->cast.expr = left_expression;
            cast->cast.to = try(parse_type(ctx));
            left_expression = cast;
            continue;
        }

        int left, right;
        operator_precedence(operator_token->kind, &left, &right);

        if (left == -1) { break; }

        if (left < precedence) { break; }

        consume_token(ctx);

        Expr *right_expression = try(parse_expr_prec(ctx, parsed, right));

        Expr *operator = malloc(sizeof(Expr));
        operator->kind = ExprKind_BinOp;

        switch (operator_token->kind) {
            case TokenKind_KeywordAnd:  operator->bin_op.kind = BinOpKind_And; break;
            case TokenKind_KeywordOr:   operator->bin_op.kind = BinOpKind_Or; break;
            case TokenKind_LessThan:    operator->bin_op.kind = BinOpKind_CmpLt; break;
            case TokenKind_GreaterThan: operator->bin_op.kind = BinOpKind_CmpGt; break;
            case TokenKind_Equality:    operator->bin_op.kind = BinOpKind_CmpEq; break;
            case TokenKind_Inequality:  operator->bin_op.kind = BinOpKind_CmpNotEq; break;
            case TokenKind_Equals:      operator->bin_op.kind = BinOpKind_Assign; break;
            case TokenKind_Plus:        operator->bin_op.kind = BinOpKind_Add; break;
            case TokenKind_Dash:        operator->bin_op.kind = BinOpKind_Sub; break;
            case TokenKind_Star:        operator->bin_op.kind = BinOpKind_Mul; break;
            case TokenKind_Slash:       operator->bin_op.kind = BinOpKind_Div; break;
            default: chn_error("Unhandled operator");
        }

        operator->bin_op.left = left_expression;
        operator->bin_op.right = right_expression;

        left_expression = operator;
    }

    return left_expression;
}

static Expr* parse_expr(ParserCtx *ctx, bool *parsed) {
    return parse_expr_prec(ctx, parsed, 0);
}

static Stmt* parse_stmt(ParserCtx *ctx) {
    Stmt *stmt = malloc(sizeof(Stmt));

    switch (current_token(ctx)->kind) {
        case TokenKind_KeywordConst:
        case TokenKind_KeywordFn: {
            stmt->kind = StmtKind_Item;
            stmt->item = try(parse_item(ctx));
            break;
        }

        default: {
            stmt->kind = StmtKind_Expr;
            stmt->expr = try(parse_expr(ctx, null));
          
            if (consume_if(ctx, TokenKind_Semicolon)) {
                // nothing
            } else {
                stmt->kind = StmtKind_NakedExpr;
            }

            break;
        }
    }

    return stmt;
}

static bool parse_fn_signature(ParserCtx *ctx, FnSig *fn_sig) {
    try(expect_token(ctx, TokenKind_LParen));

    fn_sig->parameters = dynarray_init();
    while (current_token(ctx)->kind != TokenKind_RParen) {
        FnParam parameter;

        Token *name_token = try(expect_token(ctx, TokenKind_Symbol));
        parameter.name = name_token->span;

        try(expect_token(ctx, TokenKind_Colon));

        parameter.type = try(parse_type(ctx));

        dynarray_push(fn_sig->parameters, &parameter);

        if (current_token(ctx)->kind != TokenKind_Comma) {
            break;
        }
        consume_token(ctx);
    }

    try(expect_token(ctx, TokenKind_RParen));

    if (current_token(ctx)->kind == TokenKind_Arrow) {
        consume_token(ctx);
        fn_sig->return_type = try(parse_type(ctx));
    } else {
        Ast_Type *void_type = malloc(sizeof(Ast_Type));
        void_type->kind = AstTypeKind_Void;
        fn_sig->return_type = void_type;
    }

    return true;
}

static bool parse_fn_definition(ParserCtx *ctx, Item *item) {
    // eat 'fn'
    consume_token(ctx);

    item->kind = ItemKind_FnDef;

    Token *name_tok = try(expect_token(ctx, TokenKind_Symbol));
    item->name = name_tok->span;

    FnDef *fn_decl = &item->fn_definition;

    try(parse_fn_signature(ctx, &fn_decl->signature));

    // TODO: make parse_block
    if (current_token(ctx)->kind != TokenKind_LBrace) {
        Token *token = current_token(ctx);
        errors_new(
            &ctx->module->errors,
            ctx->module->path,
            token->position,
            token->span,
            "Function body must be a block"
        );
        return null;
    }

    fn_decl->body = try(parse_primary_expr(ctx, null));

    return true;
}

static bool parse_constant(ParserCtx *ctx, Item *item) {
    // eat 'const'
    consume_token(ctx);

    item->kind = ItemKind_Const;

    Token *name_tok = try(expect_token(ctx, TokenKind_Symbol));
    item->name = name_tok->span;

    Constant *constant = &item->constant;

    if (current_token(ctx)->kind == TokenKind_Colon) {
        consume_token(ctx);

        constant->type = try(parse_type(ctx));
    } else {
        constant->type = null;
    }

    try(expect_token(ctx, TokenKind_Equals));

    constant->value = try(parse_expr(ctx, null));

    try(expect_semicolon(ctx));

    return true;
}

static bool parse_use(ParserCtx *ctx, Item *item) {
    // eat 'use'
    consume_token(ctx);

    item->kind = ItemKind_Use;

    Use *use = &item->use;
    use->path = dynarray_init();

    while (true) {
        Token *sym_tok = try(expect_token(ctx, TokenKind_Symbol));
        dynarray_push(use->path, &sym_tok->span);

        if (consume_if(ctx, TokenKind_Slash)) {
            continue;
        }

        break;
    }

    item->name = dynarray_last(item->use.path);

    try(expect_semicolon(ctx));


    return true;
}

static Item* parse_item(ParserCtx *ctx) {
    Item *item = malloc(sizeof(Item));

    if (current_token(ctx)->kind == TokenKind_KeywordPub) {
        consume_token(ctx);
        item->visibility.is_pub = true;
    } else {
        item->visibility.is_pub = false;
    }

    switch (current_token(ctx)->kind) {
        case TokenKind_KeywordFn: {
            try(parse_fn_definition(ctx, item));

            break;
        }

        case TokenKind_KeywordExtern: {
            consume_token(ctx);
            try(expect_token(ctx, TokenKind_KeywordFn));

            item->kind = ItemKind_ExternFn;
            Token *name_tok = try(expect_token(ctx, TokenKind_Symbol));
            item->name = name_tok->span;

            try(parse_fn_signature(ctx, &item->extern_fn.signature));
            try(expect_token(ctx, TokenKind_Semicolon));

            break;
        }

        case TokenKind_KeywordConst: {
            try(parse_constant(ctx, item));

            break;
        }

        case TokenKind_KeywordUse: {
            try(parse_use(ctx, item));

            // keep track of imports
            ModImport import = { .use = &item->use };
            dynarray_push(ctx->module->imports, &import);

            break;
        }
        
        default: {
            Token *token = current_token(ctx);
            errmsg_id err = errors_new(
                &ctx->module->errors,
                ctx->module->path,
                token->position,
                token->span,
                "expected item, found %s",
                token_string(current_token(ctx)->kind)
            );
            errors_add_hint(ctx->module->errors, err, "expected item (e.g. fn, struct)");

            return null;
        }
    }

    return item;
}

static AstRoot* parse_root(ParserCtx *ctx) {
    AstRoot *root = malloc(sizeof(AstRoot));

    root->items = dynarray_init();

    while (current_token(ctx)->kind != TokenKind_Eof) {
        Item *item = try(parse_item(ctx));
        dynarray_push(root->items, &item);
    }

    return root;
}

void parse_module(Module *module) {
    ParserCtx ctx;
    ctx.module = module;
    ctx.token_index = 0;

    AstRoot *root = parse_root(&ctx);
    
    if (root == null) {
        if (dynarray_len(module->errors) == 0) {
            chn_error("error list should be filled");
        }

        return;
    }

    module->ast = root;

}

