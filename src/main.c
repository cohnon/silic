#include "compiler.h"

#include <chnlib/str.h>
#include <chnlib/logger.h>
#include <stdio.h>
#include <string.h>


#define VERSION_MAJOR 0
#define VERSION_MINOR 0
#define VERSION_PATCH 0

static void print_usage(char *command) {
    fprintf(stderr,
        "\nUsage: %s <code>.sil\n\n"
        "Other Options:\n"
        "--version\t\tprints version\n"
        "--output <outfile>\tsets output file\n"
        "--build\t\t\tbuild the C(IR)\n"
        "--debug\t\t\tshow debug info\n\n",
        command
    );
}

i32 main(int argc, char **argv) {
    char *arg0 = argv[0];
    char *in_file_path = null;
    char *out_file_path = "output";
    bool build = false;
    bool debug_info = false;

    for (isize i = 1; i < argc; i++) {
        char *arg = argv[i];

        if ((arg[0] == '-') and (arg[1] == '-')) {
            if (strcmp(arg, "--version") == 0) {
                printf("%d.%d.%d\n", VERSION_MAJOR, VERSION_MINOR, VERSION_PATCH);
                return 0;
            } else if(strcmp(arg, "--output") == 0) {
                i += 1;
                out_file_path = argv[i];
	    } else if (strcmp(arg, "--build") == 0) {
		build = true;
            } else if (strcmp(arg, "--debug") == 0) {
                debug_info = true;
            } else {
                print_usage(arg0);
                return EXIT_FAILURE;
            }
        } else if (in_file_path == 0) {
            in_file_path = arg;
        } else {
            print_usage(arg0);
            return EXIT_FAILURE;
        }
    }

    if ((in_file_path == 0) or (out_file_path == 0)) {
        print_usage(arg0);
        return EXIT_FAILURE;
    }


    String path = str_from_cstr(in_file_path);

    Compiler compiler;
    compiler_init(&compiler, build, debug_info);

    compiler_compile(&compiler, path);

    compiler_deinit(&compiler);

    return EXIT_SUCCESS;
}
