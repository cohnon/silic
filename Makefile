CFILES = $(wildcard src/*.c src/*/*.c)
OFILES = $(patsubst src/%.c, build/%.o, $(CFILES))

CHNLIB_CFILES = $(wildcard libs/chnlib/src/*.c libs/chnlib/src/*/*.c)
CHNLIB_OFILES = $(patsubst libs/chnlib/src/%.c, build/chnlib/%.o, $(CHNLIB_CFILES))

FIR_CFILES = $(wildcard libs/fir/src/*.c libs/fir/src/*/*.c)
FIR_OFILES = $(patsubst libs/fir/src/%.c, build/fir/%.o, $(FIR_CFILES))

CC = gcc
FLAGS = -g -std=c11 -Wall -Wextra -pedantic -Wno-error=sizeof-pointer-memaccess
INCLUDES = -Ilibs/chnlib/include -Ilibs/fir/include

.Phony: all
all: build/silic

build/silic: $(OFILES) build/chnlib.a build/fir.a
	$(CC) $(FLAGS) $^ -o $@

build/%.o: src/%.c
	mkdir -p $(@D)
	$(CC) -c $^ $(FLAGS) $(INCLUDES) -Isrc -o $@

build/chnlib.a: $(CHNLIB_OFILES)
	ar -rc $@ $^

build/chnlib/%.o: libs/chnlib/src/%.c
	mkdir -p $(@D)
	$(CC) -c $^ $(FLAGS) $(INCLUDES) -o $@

build/fir.a: $(FIR_OFILES)
	ar -rc $@ $^

build/fir/%.o: libs/fir/src/%.c
	mkdir -p $(@D)
	$(CC) -c $^ $(FLAGS) $(INCLUDES) -o $@

.Phony: clean
clean:
	-rm -r build
