CFILES = $(wildcard src/*.c src/*/*.c)
OFILES = $(patsubst src/%.c, build/%.o, $(CFILES))

CHNLIB_CFILES = $(wildcard libs/chnlib/src/*.c libs/chnlib/src/*/*.c)
CHNLIB_OFILES = $(patsubst libs/chnlib/src/%.c, build/chnlib/%.o, $(CHNLIB_CFILES))

CC = gcc
FLAGS = -g -std=c11 -Wall -Wextra -pedantic -Wno-error=sizeof-pointer-memaccess
INCLUDES = -Ilibs/chnlib/include

.Phony: all
all: silic

silic: $(OFILES) build/chnlib.a
	$(CC) $(FLAGS) $^ -o $@

build/%.o: src/%.c
	mkdir -p $(@D)
	$(CC) -c $^ $(FLAGS) $(INCLUDES) -Isrc -o $@

build/chnlib.a: $(CHNLIB_OFILES)
	ar -rc $@ $^

build/chnlib/%.o: libs/chnlib/src/%.c
	mkdir -p $(@D)
	$(CC) -c $^ $(FLAGS) $(INCLUDES) -o $@

.Phony: clean
clean:
	-rm -r silic build
